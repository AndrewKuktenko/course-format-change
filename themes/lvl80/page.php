<?
/**
 * Template Name: Payment Success
 */

the_post();
?>
<div class="page-box">
    <div class="container">
        <h2 class="page-box__title"><? the_title() ?></h2>
        <h2 class="page-box__content"><? the_content() ?></h2>
    </div>
</div>