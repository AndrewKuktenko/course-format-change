<? use Roots\Sage\Assets; ?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/visual-bg.jpg') ?>');" class="visual">
    <div class="container">
        <div class="visual__inner">
            <h2 class="h1 visual__title">Практическая школа <br>онлайн-маркетинга</h2>
            <div class="visual__text">После 3-х месяцев обучения Вы уже специалист, а не начинающий маркетолог!</div><a href="#section-cources" class="btn scroll-link visual__button">Чему я научусь</a>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container">
        <h2 class="advantages__title">Чем наша школа отличается от других</h2>
        <div class="advantages__list">
            <div class="advantages__list-item">
                <div class="advantages__item">
                    <div class="advantages__item-image">
                        <img src="<?= Assets\asset_path('images/temp/advantages-img-2.svg') ?>" alt="" width="100" height="100">
                    </div>
                    <div class="advantages__item-content">
                        <h3 class="advantages__item-title">Реальные проекты</h3>
                        <div class="advantages__item-text">Вы будете работать с реальными клиентами, которые раскроют вам данные и проблемы своего проекта. А вы им поможете с рекомендациями в онлайн-продвижении.</div>
                    </div>
                </div>
            </div>
            <div class="advantages__list-item">
                <div class="advantages__item">
                    <div class="advantages__item-image">
                        <img src="<?= Assets\asset_path('images/temp/advantages-img-1.svg') ?>" alt="" width="100" height="100">
                    </div>
                    <div class="advantages__item-content">
                        <h3 class="advantages__item-title">Профильные специалисты</h3>
                        <div class="advantages__item-text">Вы будете учиться у лучших. Все преподаватели более 10 лет специализируются на своем направлении и знают все его тонкости. Они с широкой душой делятся опытом и секретами.</div>
                    </div>
                </div>
            </div>
            <div class="advantages__list-item">
                <div class="advantages__item">
                    <div class="advantages__item-image">
                        <img src="<?= Assets\asset_path('images/temp/advantages-img-3.svg') ?>" alt="" width="100" height="100">
                    </div>
                    <div class="advantages__item-content">
                        <h3 class="advantages__item-title">Комплексные знания</h3>
                        <div class="advantages__item-text">Вы научитесь думать стратегически, а не делать точечные вбросы. Все ваши действия будут нацелены на экономический результат, который вы сможете прогнозировать и измерять.</div>
                    </div>
                </div>
            </div>
            <div class="advantages__list-item">
                <div class="advantages__item">
                    <div class="advantages__item-image">
                        <img src="<?= Assets\asset_path('images/temp/advantages-img-4.svg') ?>" alt="" width="100" height="100">
                    </div>
                    <div class="advantages__item-content">
                        <h3 class="advantages__item-title">Обратная связь</h3>
                        <div class="advantages__item-text">Вы все время будете закреплять свои знания практикой. Поэтому мы на связи, чтобы проверить ваши домашние задания, ответить на вопросы, дать рекомендации, проверить еще раз до состояния “отлично”.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="why-box">
    <div class="container">
        <div class="why-box__wrap">
            <div class="why-box__content">
                <h2 class="why-box__title">Зачем мы основали школу?</h2>
                <div class="why-box__text">
                    <p>На рынке СНГ есть большая проблема в качественном образовании. Интернет-маркетингу в ВУЗах не научишься, статьи не рассказывают базовую информацию и часто написаны сложным языком, а большинство онлайн курсов не включают в себя ключевые инструменты
                        интернет-маркетинга.</p>
                    <p>Мы создали эту школу, чтоб передать все свои знания. Научить людей правильно продвигать продукты и сервисы в интернете. И, прежде всего, думать стратегически!</p>
                </div>
                <div class="why-box__info">
                    <div class="why-box__info-item why-box__info-item_author">Виктор Локотков и Антон Липский</div>
                    <div class="why-box__info-item">основатели школы LVL80</div>
                </div><a href="#section-cources" class="btn scroll-link why-box__button">Узнать больше о курсах</a>
            </div>
            <div class="why-box__image">
                <img src="<?= Assets\asset_path('images/temp/founders.jpg') ?>" alt="" class="why-box__image-i why-box__image-i_big" />
                <img src="<?= Assets\asset_path('images/temp/founders-small.jpg') ?>" alt="" class="why-box__image-i why-box__image-i_small" />
            </div>
        </div>
    </div>
</div>

<? get_template_part('templates/kursy', 'list') ?>

<? get_template_part('templates/reviews') ?>

<? get_template_part('templates/contacts') ?>

