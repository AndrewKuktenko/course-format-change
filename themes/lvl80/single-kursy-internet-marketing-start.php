<?
use Roots\Sage\Assets;

global $disable_free_places;
$disable_free_places = true;
?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/im-start-promo-cover.jpg') ?>');" class="promo promo_pink promo_simple">
	<div class="container">
		<div class="promo__inner">
			<div class="promo__content">
				<h2 class="h1 promo__title">Интернет-маркетинг. Начало</h2>
				<div class="promo__text">
					<p>Вы хотите поменять профессию, и не знаете подойдет ли вам интернет-маркетинг? Или вы – бизнесмен, и вам хочется понять о чем говорят интернет-маркетологи?</p>
					<p>Мы создали курс по изучению самых базовых знаний в онлайн-маркетинге, который всего за шесть дней даст вам понимание азов профессии. После этого курса вы будете говорить с интернет-маркетологами на одном языке!</p>
				</div>
                <div class="promo__label"><strong>Получите запись курса</strong> <? the_field('start_date') ?></div>
				<? get_template_part('templates/kursy', 'promo__btns') ?>
			</div>
		</div>
	</div>
</div>
<div class="info-box">
	<div class="container">
		<div class="info-box__wrap">
			<div class="info-box__content">
				<div class="info-box__text">
					<p>Ваши первые шаги в online-marketing начинаются тут! Вы разберете основные каналы продвижения в интернете и принципы их работы за шесть дней! На курсе вы изучите основные понятия и термины интернет-маркетинга, поймете с чего начинать продвижение онлайн.</p>
					<p>Мы разберем все тонкости работы поисковиков, контекстной рекламы и контент-маркетинга, а также социальные сети и видеосервисы, их фишки и особенности. Изучим все online-каналы привлечения клиентов, основы аналитики, SMM и PR. Мы расскажем вам о программах лояльности и возврате клиентов, рассылках и мессенджерах. Вы поймете как это работает и как будет приносить вам прибыль.</p>
				</div><a href="#section-schedule" class="btn info-box__button scroll-link">Просмотреть программу курса</a>
			</div>
			<div class="info-box__aside">
				<div class="info-box__image">
					<img src="<?= Assets\asset_path('images/temp/internet-marketing-image.jpg') ?>" alt="" class="info-box__image-i" />
				</div>
			</div>
		</div>
	</div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
	<div class="container">
		<div class="classes__list">
			<div class="classes__list-item"><a data-toggle="collapse" href="#class-internet-marketing-start" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-11.svg') ?>" alt="" width="100" height="100" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Интернет-маркетинг. Начало</span><span class="classes__item-label"><strong>Получите запись курса</strong> <? the_field('start_date') ?></span></span></a>
				<div
					id="class-internet-marketing-start" class="classes__content collapse in">
					<hr class="no-offset-top">
					<div class="classes__content-wrap">
						<div class="classes__content-title">Данный курс создан для:</div>
					</div>
					<div class="info-list">
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-4.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Обычным людям,</div>
								<div class="info-list__text">
									<p>которые хотят разобраться в online-маркетинге</p>
									<p><strong>После прохождения курса:</strong>
										<br>вы поймете созданы ли для профессии маркетолога и как сделать в ней первые шаги</p>
								</div>
							</div>
						</div>
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-15.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Офис-менеджерам,</div>
								<div class="info-list__text">
									<p>которым надоело просиживать стул в офисе</p>
									<p><strong>После прохождения курса:</strong>
										<br>вы овладеете азами новой профессии и получите возможности для профессионального и карьерного роста</p>
								</div>
							</div>
						</div>
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-19.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Студентам,</div>
								<div class="info-list__text">
									<p>которые параллельно с учебой хотят освоить актуальную и прибыльную профессию</p>
									<p><strong>После прохождения курса:</strong>
										<br>вы получите шанс зарабатывать параллельно с обучением в ВУЗе и нарабатывать ценный опыт в digital</p>
								</div>
							</div>
						</div>
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Владельцам бизнеса,</div>
								<div class="info-list__text">
									<p>которые хотят контролировать подрядчиков и говорить с интернет-маркетологами на одном языке</p>
									<p><strong>После прохождения курса:</strong>
										<br>вы получите все необходимые знания для постановки целей подрядчикам и контроля их эффективности в ваших проектах</p>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Длительность занятий</div>
						<div class="classes__info">
							<div class="classes__info-item">
								<div class="classes__info-title"><? the_field('start_date') ?></div>
								<div class="classes__info-text">Курс в записи</div>
							</div>
							<div class="classes__info-item">
								<div class="classes__info-title">1 видео</div>
								<div class="classes__info-text">каждые 2 дня</div>
							</div>
							<div class="classes__info-item">
								<div class="classes__info-title">Домашки</div>
								<div class="classes__info-text">в каждом занятии</div>
							</div>
							<div class="classes__info-item">
								<div class="classes__info-title">6 занятий</div>
								<div class="classes__info-text">по 1,5 - 2 часа</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Чем этот курс отличается от других?</div>
					</div>
					<div class="info-list info-list_simple">
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-16.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Запись всех лекций</div>
								<div class="info-list__text">Все лекции записаны на видео. Студенты могут пересматривать их после окончания курса в любой момент.</div>
							</div>
						</div>
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-17.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Поддержка 24/7</div>
								<div class="info-list__text">Есть возможность дополнительно заказать поддержку лекторов курса, проверку домашних заданий и обратную связь</div>
							</div>
						</div>
						<div class="info-list__item">
							<div class="info-list__image">
								<img src="<?= Assets\asset_path('images/temp/info-icon-18.svg') ?>" alt="" class="info-list__image-i" />
							</div>
							<div class="info-list__content">
								<div class="info-list__title">Доступность каждому</div>
								<div class="info-list__text">Цена настолько доступна, что получить курс может каждый. Информация подходит как новичкам, так и тем, кто уже немного занимаеться интернет-маркетингом</div>
							</div>
						</div>
					</div>
					<div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Оформить заявку</a>
					</div>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Чему вы научитесь на курсе:</div>
					</div>
					<div class="num-list num-list_pink">
						<div class="num-list__item">
							<div class="num-list__num">1</div>
							<div class="num-list__title">Основы основ</div>
							<div class="num-list__text">
								<p>Изучите основные понятия и термины интернета и интернет-маркетинга. Поймете как работает интернет, интернет-сайт, интернет-браузер и из чего они состоят. Исследуете e-mail сервисы и мессенджеры. Узнаете главное о мобильных устройствах и особенностях их использования. Мы расскажем с чего начинать продвижение онлайн и всем ли нужен сайт. Вы поймете цели создания сайта и интернет рекламы. Узнаете для чего клиент заходит на сайт, как правильно выбрать тип и название сайта и хостинга, и с помощью каких сервисов можно легко создать сайт. Изучите основы Usability, тонкости мобильной версии сайта. </p>
								<p><strong>Результат:</strong>
									<br>вы получите полное понимание процессов интернет-продвижения и первые инструменты для работы в online.</p>
							</div>
						</div>
						<div class="num-list__item">
							<div class="num-list__num">2</div>
							<div class="num-list__title">Основные каналы привлечения клиентов</div>
							<div class="num-list__text">
								<p>Мы расскажем как работает поиск Google и из чего он состоит, поделимся эволюцией поисковых систем, основами SEO и контекстной рекламы. Вы поймете что такое контент-маркетинг, что действительно влияет на трафик и бюджет и как быстро стартовать в контекстной рекламе. Изучите принципы работы популярных социальных сетей, создадите страницу на Facebook и бизнес-профиль в Instagram. Поймете что такое платная реклама и как она работает. Научитесь создавать крутую графику без помощи дизайнера.</p>
								<p><strong>Результат:</strong>
									<br>бизнес-страница на Facebook и бизнес-профиль в Instagram, понимание как работает интернет-реклама.</p>
							</div>
						</div>
						<div class="num-list__item">
							<div class="num-list__num">3</div>
							<div class="num-list__title">Другие каналы привлечения клиентов и основы аналитики</div>
							<div class="num-list__text">
								<p>Разберем PR и медийную рекламу, рекламные и тизерные сети. Изучим основы Google Analytics. Вы научитесь оценивать эффективность рекламных кампаний, поймете  какие метрики важны и как оценивать эффективность вашего сайта. Подробно разберем Webvisor и Heatmap.</p>
								<p><strong>Результат:</strong>
									<br>у вас появится широкое понимание инструментов интернет-рекламы и первый опыт оценки эффективности кампании.</p>
							</div>
						</div>
						<div class="num-list__item">
							<div class="num-list__num">4</div>
							<div class="num-list__title">Программа лояльности и возврат клиентов</div>
							<div class="num-list__text">
								<p>Поработаем со списками аудиторий, ее сегментацией и интересами. Разберем триггерные рассылки. На примерах сравним e-mail marketing с e-mail рассылкой. Расскажем что такое ремаркетинг и как он работает. Создадим вашу первую рассылку. Подробно разберем мессенджеры – самый эффективный на сегодня инструмент работы с клиентами.</p>
								<p><strong>Результат:</strong>
									<br>вы поймете как определять свою целевую аудиторию и правильно работать с ней, а также получите самый актуальный инструмент продвижения в интернете.</p>
							</div>
						</div>
					</div>
					<hr>
					<div id="section-schedule" class="classes__content-wrap">
						<div class="classes__content-title">Программа курса:</div>
					</div>
					<div class="schedule-list schedule-list_col-1">
						<div class="schedule-list__column">
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 1. Основа основ</div>
								<div class="schedule-list__text">
									<p>Основные понятия и термины интернета и интернет-маркетинга</p>
									<p>Как работает интернет, интернет-сайт, интернет-браузер и из чего они состоят</p>
									<p>Дополнительные каналы выхода в интернет: email клиенты и мессенджеры</p>
									<p>Мобильные устройства. Особенности их использования</p>
									<p>Как люди используют интернет и как этим пользоваться</p>
									<p>С чего начинать продвижение онлайн и нужен ли сайт</p>
								</div>
							</div>
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 2. Веб-сайт. Usability. Особенности создания.</div>
								<div class="schedule-list__text">
									<p>Цель создания сайта и цель интернет рекламы</p>
									<p>Для чего клиент заходит на сайт</p>
									<p>Выбор названия сайта и хостинга</p>
									<p>С помощью каких сервисов можно легко создать сайт</p>
									<p>Какие бывают типы сайтов</p>
									<p>Как оценить качество разработанного сайта до его запуска</p>
									<p>Основы Usability</p>
									<p>Мобильная версия сайта. Зачем она вообще</p>
									<p>Какие основные ошибки допускают при создании сайтов</p>
								</div>
							</div>
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 3. Основные каналы привлечения клиентов. SEO и контекстная реклама</div>
								<div class="schedule-list__text">
									<p>Как работает поиск Google и из чего он состоит</p>
									<p>Эволюция поиска и что мы видим сейчас</p>
									<p>Чем SEO отличается от контекстной рекламы</p>
									<p>Основы SEO, Что действительно важно учитывать</p>
									<p>Основы контекстной рекламы. Что влияет на траффик и бюджет</p>
									<p>Google Express или как быстро стартовать в контекстной рекламе</p>
									<p>Контент-маркетинг. Что это и как он работает</p>
								</div>
							</div>
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 4. Соцсети и видеосервисы</div>
								<div class="schedule-list__text">
									<p>Принцип работы социальных сетей</p>
									<p>Особенности самых популярных социальных сетей</p>
									<p>Создаем страницу на Facebook и бизнес профиль в Instagram</p>
									<p>Что такое платная реклама и как она работает</p>
									<p>Как быстро и качественно запустить рекламу в соц сетях</p>
									<p>Создание графики для социальных сетей</p>
									<p>Что работает в социальных сетях</p>
								</div>
							</div>
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 5. Другие каналы привлечения клиентов и основы аналитики</div>
								<div class="schedule-list__text">
									<p>Медийная реклама</p>
									<p>PR</p>
									<p>Рекламные и тизерные сети</p>
									<p>Основы Google Analytics</p>
									<p>Как оценивать эффективность рекламных кампаний. Какие метрики важны</p>
									<p>Как оценивать эффективность вашего сайта. Webvisor и Heatmap</p>
								</div>
							</div>
							<div class="schedule-list__item">
								<div class="schedule-list__item-icon"></div>
								<div class="schedule-list__title">Лекция 6. Программа лояльности и возврат клиентов</div>
								<div class="schedule-list__text">
									<p>E-mail marketing vs E-mail рассылка</p>
									<p>Список контактов, аудитория, интересы и сегментация, триггерные рассылки, создаем первую рассылку</p>
									<p>Ремаркетинг. Что это и как он работает</p>
									<p>Мессенджеры – новый эффективный инструмент работы с клиентами</p>
								</div>
							</div>
						</div>
					</div>
					<div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Записаться на курс</a>
					</div>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Кто ведет курс</div>
						<div class="classes__content-text">В нашей школе преподают только успешные практики – это наша принципиальная позиция.</div>
					</div>
					<div class="teachers">
						<div class="teachers__item">
							<div class="teachers__item-inner">
								<div class="teachers__aside">
									<div class="teachers__image">
										<img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i" />
									</div>
									<a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
									</a>
								</div>
								<div class="teachers__content">
									<div class="teachers__title">Виктор Локотков</div>
									<div class="teachers__text">Со-основатель школы LVL80.PRO. Более 10 лет опыта в интернет маркетинге. Ментор в Google Launchpad. Спикер на более чем 100 конференциях по бизнесу и маркетингу. Работал руководителем отдела маркетинга в Genius Marketing, запустил сервис Eda.ua на рынке Украины.
									</div>
								</div>
							</div>
						</div>
						<div class="teachers__item">
							<div class="teachers__item-inner">
								<div class="teachers__aside">
									<div class="teachers__image">
										<img src="<?= Assets\asset_path('images/temp/teachers/anton-lipsky.jpg') ?>" alt="" class="teachers__image-i" />
									</div>
									<a href="https://www.facebook.com/lipskiy.anton/" target="_blank" class="teachers__social">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
									</a>
								</div>
								<div class="teachers__content">
									<div class="teachers__title">Антон Липский</div>
									<div class="teachers__text">Со-основатель школы LVL80.PRO. Один из лучших специалистов по контекстной рекламе и аналитике на рынке Украины. Работал как с малым бизнесом, так и с крупными международными брендами, среди которых L’Oreal, Samsung, Nestle и другие.</div>
								</div>
							</div>
						</div>
						<div class="teachers__item">
							<div class="teachers__item-inner">
								<div class="teachers__aside">
									<div class="teachers__image">
										<img src="<?= Assets\asset_path('images/temp/teachers/zhenya-dubrova-aliksyuk.jpg') ?>" alt="" class="teachers__image-i" />
									</div>
									<a href="https://www.facebook.com/Zhenya.Aliksyuk" target="_blank" class="teachers__social">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
									</a>
								</div>
								<div class="teachers__content">
									<div class="teachers__title">Евгения Дуброва-Аликсюк</div>
									<div class="teachers__text">
										<p>Специалист по контекстной рекламе. Более 5 лет опыта на разных типах проектов. Автор популярного канала в Telegram о PPC и веб-аналитике, лектор на профильных мероприятиях (в т.ч. и мероприятиях Google).</p>
									</div>
								</div>
							</div>
						</div>
						<div class="teachers__item">
							<div class="teachers__item-inner">
								<div class="teachers__aside">
									<div class="teachers__image">
										<img src="<?= Assets\asset_path('images/temp/teachers/nikita-yabloko.jpg') ?>" alt="" class="teachers__image-i" />
									</div>
									<a href="https://www.facebook.com/appleseater" target="_blank" class="teachers__social">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
									</a>
								</div>
								<div class="teachers__content">
									<div class="teachers__title">Никита Яблоко</div>
									<div class="teachers__text">Social Media Manager в YODEZEEN. Опыт работы в SMM – 3 года, преподавательский стаж – 1 год. Клиенты: Freshline, Nebbia, YODEZEEN, Creamoire, Merida, Neonail, Хуторок.</div>
								</div>
							</div>
						</div>
					</div>
					<div class="teachers__item">
							<div class="teachers__item-inner">
								<div class="teachers__aside">
									<div class="teachers__image">
										<img src="<?= Assets\asset_path('images/temp/teachers/julia-davidenko.png') ?>" alt="" class="teachers__image-i" />
									</div>
									<a href="https://www.facebook.com/j.davydenko" target="_blank" class="teachers__social">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
									</a>
								</div>
								<div class="teachers__content">
									<div class="teachers__title">Юлия Давиденко</div>
									<div class="teachers__text">Специалист по таргетированной рекламе в LVL80. FB-маркетолог. 4 года практики в интернет-маркетинге. Запуск рекламных кампаний на Украину, Россию, США, Европу, пространство СНГ. То что вы оказались на этом сайте - это тоже ее работа :)</div>
								</div>
							</div>
						</div>
					<div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Оформить заявку</a>
					</div>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Отзывы студентов</div>
						<div class="classes__content-text">Ученики, которые уже прошли этот курс, оставили нам отзывы. Смотрите!</div>
					</div>
					<div class="reviews-slider">
						<div class="reviews-slider__box">
							<div class="reviews-slider__box-inner"><a href="https://youtu.be/QF_9v1XG1as" data-fancybox="video" style="background-image: url('<?= Assets\asset_path('images/temp/reviews-cover-1.jpg') ?>');" class="reviews-slider__item"><span class="reviews-slider__item-icon"></span></a>
								<div class="reviews-slider__label"><strong>Ястремский Владимир</strong>, CMO BeerMe</div>
								<div class="reviews-slider__social">
									<a href="https://www.facebook.com/yastremskiyva" target="_blank" class="reviews-slider__social-link">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews-slider__social-image" />
									</a>
								</div>
							</div>
						</div>
						<div class="reviews-slider__box">
							<div class="reviews-slider__box-inner">
								<div class="reviews-slider__photo">
									<img src="<?= Assets\asset_path('images/temp/inna-khanas.jpg') ?>" alt="" class="reviews-slider__photo-image" />
								</div>
								<div class="reviews-slider__message">С нуля выучить основы интернет-маркетинга будучи студентом вообще другой специальности - легко!) Спасибо за отличный курс, бонусные лекции и крутых спикеров) Антон и Виктор, вы супер) много полезной информации с практическим применением! К тому же, сразу же после обучения начала работать. Что может быть лучше?)</div>
								<div class="reviews-slider__label"><strong>
Инна Ханас</strong></div>
								<div class="reviews-slider__social">
									<a href="https://www.facebook.com/profile.php?id=100006853936175" target="_blank" class="reviews-slider__social-link">
										<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews-slider__social-image" />
									</a>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div id="section-price" class="classes__content-wrap">
						<div class="classes__content-title">Стоимость курса</div>
						<div class="classes__content-text">Курс длится 6 занятий. Длительность одного занятия до 2 часов. Видео приходят вам на почту каждые 2 дня. Вы будете получать также домашние задания для закрепления знаний на практике.</div>
					</div>
					<? get_template_part('templates/kursy', 'price') ?>
					<hr>
					<div class="classes__content-wrap">
						<div class="classes__content-title">Вопросы и ответы</div>
					</div>
					<div class="classes__collapse">
						<div class="collapse-list">
							<div class="collapse-list__item">
								<div data-toggle="collapse" data-target="#faq-IMStart-1" class="collapse-list__item-title collapsed">Получу ли я доступ к видео, если не смогу быть на каждом занятии?</div>
								<div id="faq-IMStart-1" class="collapse">
									<div class="collapse-list__item-text">Да, вы сможете посмотреть все лекции, входящие в курс, а также пересматривать их уже после окончания курса.</div>
								</div>
							</div>
							<div class="collapse-list__item">
								<div data-toggle="collapse" data-target="#faq-IMStart-2" class="collapse-list__item-title collapsed">В чем разница между форматами обучения онлайн и оффлайн?</div>
								<div id="faq-IMStart-2" class="collapse">
									<div class="collapse-list__item-text">Вы получаете одинаковый объем знаний и доступ к материалам независимо от формы обучения, но именно в оффлайн-формате вы сможете задавать вопросы вживую и общаться с другими студентами.
									</div>
								</div>
							</div>
							<div class="collapse-list__item">
								<div data-toggle="collapse" data-target="#faq-IMStart-3" class="collapse-list__item-title collapsed">Будут ли практические задание после каждой лекции?</div>
								<div id="faq-IMStart-3" class="collapse">
									<div class="collapse-list__item-text">Да, вы будете получать домашние задание и сможете сразу проверять полученные знания на практике. На лекциях вы сможете задавать дополнительные вопросы.</div>
								</div>
							</div>
							<div class="collapse-list__item">
								<div data-toggle="collapse" data-target="#faq-IMStart-4" class="collapse-list__item-title collapsed">Что я получу в результате прохождения этого курса?</div>
								<div id="faq-IMStart-4" class="collapse">
									<div class="collapse-list__item-text">Вы овладеете базовыми знаниями и понятиями интернет-маркетинга, поймете насколько вам это интересно и в каком направлении вы бы хотели развиваться, чтобы стать эффективным специалистом.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts">
	<div class="container">
		<div class="contacts__inner">
			<div class="contacts__title">Еще думаете?</div>
			<div class="contacts__text">Напишите нам и мы развеем все Ваши сомнения :)</div>
			<div class="contacts__image">
				<img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
			</div>
			<div class="contacts__controls">
				<div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
				</div>
				<div class="contacts__controls-item"><a href="https://t.me/lvl80pro_bot" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
				</div>
			</div>
		</div>
	</div>
</div>