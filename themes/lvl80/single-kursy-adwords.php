<? use Roots\Sage\Assets; ?>
<div style="background-image:url(<?= Assets\asset_path('images/temp/adwords-promo-cover.jpg') ?>);" class="promo promo_green promo_simple">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Контекстная реклама в Google Ads. PRO</h2>
                <div class="promo__text">Каждый день в контекстной рекламе появляется все больше рекламодателей, но не все они понимают как правильно запускать рекламные кампании, как писать продающие заголовки, что больше всего цепляется клиентов. Мы научим вас запускать контекстную рекламу
                    максимально эффективно.</div><? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Google постоянно вводит новинки в Google Ads: обновляет интерфейс, вводит новые правила, меняет RTB систему, вводит новые плейсменты и убирает устаревшие.</p>
                    <p>На курсе <strong>Контекстная реклама в Google Ads. PRO</strong> мы разберемся во всех новинках от Google. Поговорим о том, что сейчас лучше всего работает в контекстной рекламе. Как сделать так, чтобы Ваше объявление даже на 3ей позиции получало больше
                        клиентов, чем объявление на первой позиции. Как должна выглядеть семантика здорового маркетолога.</p>
                </div><a href="#section-schedule" class="btn btn-green info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/adwords-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-adwords" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" width="92" height="92" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title" style="font-size:200%">Контекстная реклама в AdWords. PRO</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                        id="class-adwords" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-1.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Начинающим интернет-маркетологам</div>
                                <div class="info-list__text">которые хотят полностью овладеть всеми тонкостями контекстной рекламы</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-5.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Начинающим специалистам по контекстной рекламе</div>
                                <div class="info-list__text">которые хотят стать настоящими профи</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-11.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Владельцам бизнеса</div>
                                <div class="info-list__text">которые хотят четко контролировать работу интернет-маркетолога</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс состоит из <strong>10 занятий</strong>, каждую неделю проходит по 2 занятия
                            <br><span class="color-brand">(вторник и четверг в 19:30, GMT+2)</span>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                        <div class="classes__content-text">Этот курс дает не просто знания об инструменте, а понимание принципов его работы с разными типами проектов. Причем работа будет идти на живых примерах и аккаунтах. Вы будете выполнять обязательные домашние задания, за счёт которых получите еще
                            больше практики.</div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1 schedule-list_green">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Системы контекстной рекламы</div>
                                <div class="schedule-list__text">Принципы работы систем контекстной рекламы, ценообразования в них, основные задачи и потенциал</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Стратегия размещения</div>
                                <div class="schedule-list__text">Подход к планированию, создание стратегии размещения, роль контекста в стратегии бренда</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Структура рекламного аккаунта</div>
                                <div class="schedule-list__text">Построение качественной структуры аккаунта, типы рекламных кампаний, нюансы структуры под разные проекты и задачи</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Семантическое ядро</div>
                                <div class="schedule-list__text">Сбор и автоматическая генерация семантического ядра, группировка запросов по семантике, частотности и конкурентности, полезные сервисы</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Контекстные объявления</div>
                                <div class="schedule-list__text">Составление продающих объявлений, расширения объявлений, динамические вставки, тестирование объявлений</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Поисковые рекламные кампании</div>
                                <div class="schedule-list__text">Нюансы настройки и запуска поисковых кампаний, типы и функции поисковых кампаний, динамические кампании, планирование</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Дисплейные рекламные кампании</div>
                                <div class="schedule-list__text">Нюансы настройки таргетингов, частоты и охватов, планирование имиджевых кампаний, адаптивные баннера</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Реклама на YouTube</div>
                                <div class="schedule-list__text">Продвижение видео-роликов, подбор площадок, таргетингов, рекомендации к видео</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Adwords Editor (+MS Excel)</div>
                                <div class="schedule-list__text">Внесение массовых изменений в крупные кампании, разворачивание с нуля огромных аккаунтов, автозамены, подготовка данных к загрузке</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Ремаркетинговые кампании</div>
                                <div class="schedule-list__text">Настройка ремаркетинга через Google Ads и Google Analytics, динамический товарный ремаркетинг</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Оптимизация рекламных кампаний</div>
                                <div class="schedule-list__text">Принципы оптимизации, лучшие практики, пользовательские метрики для оптимизации, отслеживание конверсий и транзакций</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Автоматизация рекламных кампаний</div>
                                <div class="schedule-list__text">Автоматические правила, уведомления, использование скриптов.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">E-commerce и не e-commerce проекты</div>
                                <div class="schedule-list__text">Разница подходов к разным типам проектов, необходимый минимум, что нужно, а что нет</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Я очень трепетно подхожу к выбору преподавателей, к их компетенции и опыту, поэтому подобрал для вас лучших из лучших. Этот курс ведут только практики!</div>
                    </div>
                    <div class="teachers teachers">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/anton-lipsky.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/lipskiy.anton/" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Антон Липский</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Один из лучших специалистов по контекстной рекламе и аналитике на рынке Украины. Работал как с малым бизнесом, так и с крупными международными брендами, среди которых были L’Oreal, Samsung, Nestle и другие.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="//lvl80.pro/wp-content/uploads/2018/01/dima-tonkih.png" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/dmytro.tonkikh" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Дмитрий Тонких</div>
                                    <div class="teachers__text">Специалист по контекстной рекламе в компании ЛУН.ua. Более 5 лет практики с ТОП-рекламодателями Украины в сфере e-commerce. Один из лучших скриптовиков в Украине. Автор многих скриптов и канала в Telegram о Google Ads скриптах.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="//lvl80.pro/wp-content/uploads/2018/02/dmytro-melinyshyn.jpg" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/endemionus" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Дмитрий Мелинишин</div>
                                    <div class="teachers__text">Performance Media Head в агентстве Performics группы Publicis One Ukraine. Более 10 лет опыта работы в сфере интернет-рекламы. Работает с ТОПовыми международными и локальными брендами в рамках комплексных стратегий.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="//lvl80.pro/wp-content/uploads/2018/02/zhenya-dubrova-aliksyuk.jpg" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/Zhenya.Aliksyuk" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Евгения Дуброва-Аликсюк</div>
                                    <div class="teachers__text">Специалист по контекстной рекламе. Более 5 лет опыта на разных типах проектов. Автор популярного канала в Telegram о PPC и веб-аналитике, лектор на профильных мероприятиях (в т.ч. и мероприятиях Google).
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green scroll-link">Оформить заявку на курс</a></div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится чуть больше 1 месяца. Состоит из 10 лекций. Проходит 2 раза в неделю. Можно выбрать онлайн или офлайн обучение. Количество мест на обоих потоках ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts contacts_green">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите мне и я развею все сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lipskiy-photo.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>