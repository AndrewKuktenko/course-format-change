<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
	// Enable features from Soil when plugin is activated
	// https://roots.io/plugins/soil/
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
	add_theme_support( 'soil-jquery-cdn' );
	add_theme_support( 'soil-relative-urls' );

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations
	load_theme_textdomain( 'sage', get_template_directory() . '/lang' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus( [
		'primary' => 'Primary menu',
		'footer'  => 'Footer menu',
	] );

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );


    add_image_size('kursy-thumb', 475, 245, true);
    add_image_size('avatar', 100, 100, true);

}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

/**
 * Register sidebars
 */
function widgets_init() {
    register_sidebar( [
	    'id'            => 'right-sidebar',
	    'name'          => 'Right sidebar',
	    'before_widget' => '<div class="page-wrap__widget %1$s %2$s">',
	    'after_widget'  => '</div>'
    ] );
}

add_action( 'widgets_init', __NAMESPACE__ . '\\widgets_init' );

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
	static $display;

	isset( $display ) || $display = ! in_array( true, [
		// The sidebar will NOT be displayed if ANY of the following return true.
		// @link https://codex.wordpress.org/Conditional_Tags
		is_404(),
		is_front_page(),
		is_page_template( 'template-custom.php' ),
	] );

	return apply_filters( 'sage/display_sidebar', $display );
}

/**
 * Theme assets
 */
function assets() {
	//wp_enqueue_style( 'css/static', get_template_directory_uri() . '/static/assets/lvl80.min.css' );
	wp_enqueue_style( 'css/main', Assets\asset_path('styles/main.css') );

	wp_enqueue_script( 'sage/libs', get_template_directory_uri() . '/static/assets/lvl80.min.js', [ 'jquery' ], null, true );
	wp_enqueue_script( 'sage/core', Assets\asset_path('scripts/main.js'), [ 'jquery' ], null, true );
    wp_localize_script( 'sage/core', 'settings', [ 'ajax_url' => admin_url('admin-ajax.php') ]);
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );
