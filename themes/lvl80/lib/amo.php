<?php

function get_amo_instance()
{
	static $instance;

	if (!$instance)
	{
		$instance = new \AmoCRM\Client(getenv('AmoCRM_domain'), getenv('AmoCRM_login'), getenv('AmoCRM_pass'));
	}

	return $instance;
}


add_action('wp_ajax_nopriv_order', 'ajax_order');
add_action('wp_ajax_order', 'ajax_order');
function ajax_order()
{
    if (!isset($_POST['spam-off']) || $_POST['spam-off'] != 'on') die;

    if (
        !isset(
            $_POST['ID'],
            $_POST['field-order-name'],
            $_POST['field-order-type'],
            $_POST['field-order-email'],
            $_POST['field-order-price'],
            $_POST['field-order-phone']
        ) ||
        !filter_var($_POST['field-order-email'], FILTER_VALIDATE_EMAIL) ||
        !$post = get_post($_POST['ID'])
    )
    {
        die('invalid');
    }


	$lead_title = 'Покупка курса - ' . get_the_title($post) . ' ('.$_POST['field-order-type'].')';
	$price = preg_replace('@\D+@', '', $_POST['field-order-price']);
	switch (qtranxf_getLanguage())
	{
		case 'ru': $currency = '₽';break;
		case 'us': $currency = '$';break;
		case 'eu': $currency = '€';break;
		default:   $currency = '₴';break;
	}


	$msg[] = $lead_title . "\n";
	$msg[] = "Имя: {$_POST['field-order-name']}";
	$msg[] = "Телефон: {$_POST['field-order-phone']}";
	$msg[] = "Email: {$_POST['field-order-email']}";
	$msg[] = "Стоимость: $price $currency";
	$msg[] = "Откуда пришел: {$_SERVER['HTTP_REFERER']}";

	wp_mail( 'o.bokman@lvl80.pro', $lead_title, join( "\n", $msg ) );

	try
	{
        // Создание клиента
        $amo = get_amo_instance();

        // Получение экземпляра модели для работы с контактами
        $contact = $amo->contact;

	    $is_exists_contact = $contact->apiList(['query' => $_POST['field-order-email']]);
	    if (!$is_exists_contact)
	    {
		    // Заполнение полей модели
		    $contact['name'] = $_POST['field-order-name'];

		    //email
		    $contact->addCustomField(392495, $_POST['field-order-email'], 'PRIV');

		    //phone
		    $contact->addCustomField(392493, $_POST['field-order-phone'], 'HOME');

		    // Добавление нового контакта и получение его ID
		    $contact_id = $contact->apiAdd();
	    }
	    else
        {
		    $contact_id = $is_exists_contact[0]['id'];
	    }


        $lead = $amo->lead;

        $lead['name'] = $lead_title;
        $lead['status_id'] = 17594887;
        $lead['price'] = $price;

        //откуда пришел
        $lead->addCustomField(398473, $_SERVER['HTTP_REFERER']);

		$type = $_POST['field-order-type'] === 'online' ? 'онлайн' : 'оффлайн';
        $lead->addCustomField(461655, $type);
        $lead->addCustomField(461657, qtranxf_getLanguage());
        $lead->addCustomField(461661, $post->ID);
		$lead->addCustomField(475353, gaParseCookie());

        //валюта
        $lead->addCustomField(412837, $currency);

        $lead_id = $lead->apiAdd();

        $link = $amo->links;
        $link['from'] = 'leads';
        $link['from_id'] = $lead_id;
        $link['to'] = 'contacts';
        $link['to_id'] = $contact_id;
	    $link->apiLink();

	    $upd_lead = $amo->lead;

	    $upd_lead->addCustomField(461929, admin_url( "admin-post.php?action=callback&order_id=$lead_id" ));
	    $upd_lead->apiUpdate($lead_id, '+3 seconds');

	    $mailer = mailer_instance();
	    $mailer_group_id = get_field("id_{$type}_списка", $post->ID);

	    if (ENV !== 'DEV')
	    {
		    $mailer->post("lists/$mailer_group_id/members", [
			    'email_address' => $_POST['field-order-email'],
			    'status'        => 'subscribed',
		    ]);
	    }

		if (get_field("цена_{$type}_full", $post))
		{
			$md5_hash = md5($lead_id.'|'.time().'|@#%$6GhTYrs');

			$post_id = wp_insert_post(['post_type' => 'payment', 'post_status' => 'publish']);

			add_post_meta($post_id, 'payment_hash',   $md5_hash);
			add_post_meta($post_id, 'payment_postID', $_POST['ID']);
			add_post_meta($post_id, 'payment_leadID', $lead_id);
			add_post_meta($post_id, 'payment_locale', qtranxf_getLanguage());
			add_post_meta($post_id, 'payment_type',   $type);
			add_post_meta($post_id, 'is_paid',        false);

			wp_send_json_success(['checkout_url' => site_url("/checkout/$md5_hash/") ]);
		} else {

			$price *= 100;
			$desc   = '[Частичная оплата] Покупка курса - ' . get_the_title( $post ) . ' (' . $type . ')';

			if ($response = fondy_payment_process($lead_id, $price, $desc)) {
				wp_send_json_success(['checkout_url' => $response['checkout_url']]);
			}
		}

    }
    catch (\AmoCRM\Exception $e)
    {
        printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
    }
    die;
}

function gaParseCookie()
{
	$cid = '';
	if ( isset( $_COOKIE['_ga'] ) )
	{
		list( $version, $domainDepth, $cid1, $cid2 ) = explode( '.', $_COOKIE["_ga"] );
		$cid = $cid1 . '.' . $cid2;
	}

	return $cid;
}


add_action('wp_ajax_nopriv_call_request', 'ajax_call_request');
add_action('wp_ajax_call_request', 'ajax_call_request');
function ajax_call_request()
{
    if (count($_POST) != 3 || !isset($_POST['field-feedback-callback-name'], $_POST['field-feedback-callback-phone']))
    {
        die('invalid');
    }


    try
    {
        // Создание клиента
	    $amo = get_amo_instance();

        // Получение экземпляра модели для работы с аккаунтом
        $account = $amo->account;

        // Получение экземпляра модели для работы с контактами
        $contact = $amo->contact;

        // Заполнение полей модели
        $contact['name'] = $_POST['field-feedback-callback-name'];
        //phone
        $contact->addCustomField(392493, $_POST['field-feedback-callback-phone'], 'HOME');

        // Добавление нового контакта и получение его ID
        $contact_id = $contact->apiAdd();

        $lead = $amo->lead;
	    $lead_title = 'Запрос на обратный звонок';
        $lead['name'] = $lead_title;
        $lead['status_id'] = 17594887;
        $lead['price'] = 1;

        //откуда пришел
        $lead->addCustomField(398473, $_SERVER['HTTP_REFERER']);
        $lead_id = $lead->apiAdd();

        $link = $amo->links;
        $link['from'] = 'leads';
        $link['from_id'] = $lead_id;
        $link['to'] = 'contacts';
        $link['to_id'] = $contact_id;

        var_dump($link->apiLink());

	    $msg[] = $lead_title . "\n";
	    $msg[] = "Имя: {$contact['field-feedback-callback-name']}";
	    $msg[] = "Телефон: {$_POST['field-feedback-callback-phone']}";
	    $msg[] = "Откуда пришел: {$_SERVER['HTTP_REFERER']}";

	    wp_mail( 'o.bokman@lvl80.pro', $lead_title, join( "\n", $msg ) );

    }
    catch (\AmoCRM\Exception $e)
    {
        printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
    }
    die;
}


add_action('wp_ajax_nopriv_feedback', 'ajax_feedback');
add_action('wp_ajax_feedback', 'ajax_feedback');
function ajax_feedback()
{
	// Создание клиента
	$amo = get_amo_instance();

	// Получение экземпляра модели для работы с контактами
	$contact = $amo->contact;

	// Заполнение полей модели
	$contact['name'] = $_POST['field-feedback-message-name'];

	//email
	$contact->addCustomField(392495, $_POST['field-feedback-message-email'], 'PRIV');

	// Добавление нового контакта и получение его ID
	$contact_id = $contact->apiAdd();

	$lead = $amo->lead;
	$lead['name'] = 'Новая заявка на обратную связь';
	$lead['status_id'] = 17594887;

	//откуда пришел
	$lead->addCustomField(398473, $_SERVER['HTTP_REFERER']);

	//message
	$lead->addCustomField(428207, $_POST['field-feedback-message-text']);

	$lead_id = $lead->apiAdd();


	$link = $amo->links;
	$link['from'] = 'leads';
	$link['from_id'] = $lead_id;
	$link['to'] = 'contacts';
	$link['to_id'] = $contact_id;

	$link->apiLink();


	$note = $amo->note;
	$note['element_type'] = \AmoCRM\Models\Note::TYPE_LEAD; // 1 - contact, 2 - lead
	$note['note_type'] = \AmoCRM\Models\Note::COMMON; // @see https://developers.amocrm.ru/rest_api/notes_type.php

	$note['element_id'] = $lead_id;

	$note['text'] = $_POST['field-feedback-message-text'];

	$note->apiAdd();
}


add_action('wpcf7_before_send_mail', function ($cf7)
{
    if (!isset($cf7->posted_data) && class_exists('WPCF7_Submission'))
    {
        // Contact Form 7 version 3.9 removed $cf7->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();

        if ($submission)
        {
            $data = array();
            $data['title'] = $cf7->title();
            $data['posted_data'] = $submission->get_posted_data();
            $data['posted_data']['Page Title'] = wpcf7_special_mail_tag('', '_post_title', '');
            $data['posted_data']['Page URL'] = wpcf7_special_mail_tag('', '_post_url', '');
            $data['posted_data']['Submitted from Page URL'] = wpcf7_special_mail_tag('', '_url', '');

            // Создание клиента
	        $amo = get_amo_instance();

            // Получение экземпляра модели для работы с контактами
            $contact = $amo->contact;

            // Заполнение полей модели
            $contact['name'] = $data['posted_data']['name'];

            //email
            $contact->addCustomField(392495, $data['posted_data']['email2'], 'PRIV');

            // Добавление нового контакта и получение его ID
            $contact_id = $contact->apiAdd();

            $lead = $amo->lead;
            $lead['name'] = 'Новая заявка';
            $lead['status_id'] = 17594887;

            //откуда пришел
            $lead->addCustomField(398473, $data['posted_data']['Submitted from Page URL']);

            //message
            $lead->addCustomField(428207, $data['posted_data']['message']);

            $lead_id = $lead->apiAdd();


            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $lead_id;
            $link['to'] = 'contacts';
            $link['to_id'] = $contact_id;

            $link->apiLink();

	        $note = $amo->note;
	        $note['element_type'] = \AmoCRM\Models\Note::TYPE_LEAD; // 1 - contact, 2 - lead
	        $note['note_type'] = \AmoCRM\Models\Note::COMMON; // @see https://developers.amocrm.ru/rest_api/notes_type.php

	        $note['element_id'] = $lead_id;

	        $note['text'] = $data['posted_data']['message'];

	        $note->apiAdd();

        }
    }
});

