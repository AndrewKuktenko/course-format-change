<?php

function register_types() {

	register_post_type( 'kursy', array(
		'labels' => array(
			'name' => 'Курсы',
		),
		'public'          => true,
		'menu_position'   => 20,
		'menu_icon'       => 'dashicons-book',
		'capability_type' => 'post',
		'has_archive'     => true,
		'supports'        => array(
			'title',
			'thumbnail',
			'excerpt',
		)
	) );

	/* Disable the taxonomy archive pages */
	add_action('pre_get_posts', 'jb_disable_tax_archive');
	function jb_disable_tax_archive($qry) {

		if (is_admin()) return;

		if (is_tax('kursy-cat')){
			$qry->set_404();
		}

	}

	//flush_rewrite_rules();
}

add_action( 'init', 'register_types' );