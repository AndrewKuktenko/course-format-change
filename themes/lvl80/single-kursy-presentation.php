<? use Roots\Sage\Assets;?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/presentation-promo-cover.jpg') ?>');" class="promo promo_simple promo_violet">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Presentation Skills: <br>Подготовка. Слайды. Выступление</h2>
                <div class="promo__text">Сегодня презентации - самый популярный способ общения. А умение правильно и эффективно провести презентацию дает ключ к успешным продажам, запуску самых невероятных идей и проектов, стремительному карьерному росту. На курсе вы научитесь создавать
                    эффективные слайды и уверенно взаимодействовать с аудиторией во время презентации, добиваясь нужного результата.</div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>А если честно себе признаться, было такое?</p>
                    <p>Ты сидишь над презентацией уже битый час, а она выглядит сыро и далеко не так, как хотелось бы. Потом ты выходишь что-то презентовать и вместо уверенности и легкости ты испытываешь неловкость, ком в горле, а лицо становится похожим на помидор.</p>
                    <p>Знакомо?</p>
                    <p>Тогда этот курс для тебя! Тут ты научишься делать презентации для клиентов, руководства, команды, и всегда будешь с ними на высоте.</p>
                </div><a href="#section-schedule" class="btn btn-violet info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/presentation-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-presentation" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-7.svg') ?>" alt="" width="94" height="90" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Presentation Skills: Подготовка. Слайды. Выступление</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                    id="class-presentation" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list info-list_simple">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Маркетологам и бизнесменам</div>
                                <div class="info-list__text">Которым важно интересно, доступно и убедительно донести свои идеи и/или большие объемы информации инвесторам, клиентам, партнерам</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Специалистам</div>
                                <div class="info-list__text">(менеджерам по продаже, менеджерам по продукту) Которые хотят создавать эффективные презентации и уверенно убеждать свою аудиторию</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-5.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Начинающим спикерам</div>
                                <div class="info-list__text">Которые хотят эффективно взаимодействовать со своей аудиторией и убедительно доносить свою точку зрения</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс длится <strong>2 месяца</strong>, каждую неделю проходит по <strong>2 занятия</strong>
                            <br><span class="color-brand">(среда в 19:30 и суббота в 10:00, GMT+2)</span>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                        <div class="classes__content-text classes__content-text_align-left">Этот курс уникален, так как включает в себя все 3 блока, которые станут устойчивой платформой убедительного и уверенного выступления для каждого:
                            <br><strong>Подготовка и storytelling</strong> — научимся создавать интересные и убедительные сценарии для своих презентаций
                            <br><strong>Дизайн слайдов</strong> — освоим методики и техники объединения графического и информационного дизайна для создания доступных и простых слайдов
                            <br><strong>Выступление</strong> — начнем уверенно и эффективно взаимодействовать со своей аудиторией</div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1 schedule-list_violet">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Основные принципы дизайна презентаций</div>
                                <div class="schedule-list__text">Научимся основным принципам дизайна презентаций, рассмотрим ошибки, определимся с инструментарием и целями</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Композиция и визуализация на слайдах</div>
                                <div class="schedule-list__text">Разберемся с тем, как правильно визуализировать, структурировать и размещать большие объемы информации на слайдах. Познакомимся с доступными и понятными графиками, диаграммами, инфографикой на слайдах</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Цвета и шрифты</div>
                                <div class="schedule-list__text">Научимся направлять свою аудиторию используя шрифты и цвета в презентации. Применять основные принципы типографики, что поможет вам делать приличные слайды только с текстом</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Фото, иконки, GIF-анимация и видео в презентации</div>
                                <div class="schedule-list__text">Разберемся с тем, как правильно подобрать, быстро найти и умело использовать медиа объекты в презентациях</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Online инструменты для создания презентаций</div>
                                <div class="schedule-list__text">Освоим создание презентаций с помощью удобных онлайн сервисов</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Шаблоны</div>
                                <div class="schedule-list__text">Научимся использовать готовые шаблоны “с умом”, а также создавать свои собственные</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Тренды в дизайне 2018 и адаптация их в презентациях</div>
                                <div class="schedule-list__text">Разберемся с основными трендами в дизайне и их интеграцией в дизайн презентаций</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Подготовка к презентации</div>
                                <div class="schedule-list__text">Качественная подготовка к презентации – 90% успеха. Научимся правильно ставить цели презентации, структурировать ваши идеи, факты и данные в логическую убедительную историю</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Начало и окончание презентации</div>
                                <div class="schedule-list__text">Разберемся с тем, как расположить к себе аудиторию с самого начала презентации. Задать динамику и ритм всему выступлению. Научимся резюмировать, создавать интересную кульминацию презентации</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Убеждение vs внушение. Предоставление доказательств</div>
                                <div class="schedule-list__text">Научимся убеждать аудиторию, с помощью ярких примеров и доказательств. Будем учится качественного аргументировать суть излагаемой проблемы, используя и статистику и убедительные примеры</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Язык и жесты в презентации</div>
                                <div class="schedule-list__text">Научимся говорить на одном языке с нашей аудитории, создавать яркие, простые, понятные сообщения и управлять своим телом во время выступления</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Составление резюме и удачное собеседование</div>
                                <div class="schedule-list__text">Создадим свое резюме, которое сможет выделится из общей массы. Пройдем практику уверенных ответов на вопросы в интервью с работодателем</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Уверенность во время выступления</div>
                                <div class="schedule-list__text">Освоим самые главные принципы работы с аудиторией. Научимся уверенно выступать и влиять на свою аудиторию, отвечать на вопросы</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, к их компетенции и опыту, поэтому подобрали для вас лучших из лучших.</div>
                    </div>
                    <div class="teachers teachers_centered">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/svetlana-lebedeva.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/sveta.lebedeva.148" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Лебедева Светлана</div>
                                    <div class="teachers__text">14 лет опыта в маркетинге и продажах. Прошла карьерный путь от консультанта по продаже в маленьком рекламном агентстве до маркетинг директора в Shell Retail Ukraine. Верит в то, что маркетинг повсюду, а систематизированный подход к изучению
                                        основ продаж и маркетинга, помогает положительно влиять на все сферы жизни человека.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 2 месяца. Состоит из 18 лекций. Проходит 2 раза в неделю. Можно выбрать онлайн или офлайн обучение. Количество мест на обоих потоках ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts contacts_violet">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите мне и я развею все сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lokotkov-photo.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="http://m.me/lvl80" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>