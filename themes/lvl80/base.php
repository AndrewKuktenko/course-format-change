<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part( 'templates/head' ); ?>
<body <?php body_class(); ?>>
<?= do_shortcode('[shfs_body]') ?>
<? get_template_part( 'templates/header' ); ?>
<?php do_action( 'get_header' ); ?>
<main class="main <?= apply_filters('main_tag_class', '') ?>">
    <?php include Wrapper\template_path(); ?>
</main>
<?php
get_template_part( 'templates/footer' );
do_action( 'get_footer' );
wp_footer();
?>
</body>
</html>
