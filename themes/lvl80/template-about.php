<?
/**
 *
 * Template Name: О нас
 *
 */

use Roots\Sage\Assets;

the_post();

?>
<div class="info-box info-box_bg-grey">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <h2 class="info-box__title"><? the_title() ?></h2>
                <div class="info-box__text">
                    <? the_content() ?>
                </div>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/about-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-content">
    <div class="container">
        <h2 class="section-content__title">Преподаватели школы</h2>
        <div class="teachers teachers_mobile-offset-fix">
            <div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Виктор Локотков</div>
                        <div class="teachers__text">Со-основатель школы LVL80. Более 10 лет опыта в интернет маркетинге. Работал руководителем отдела маркетинга в Genius Marketing, где Facebook является основным источником привлечения клиентов.</div>
                    </div>
                </div>
            </div>
            <div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/anton-lipsky.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/lipskiy.anton/" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Антон Липский</div>
                        <div class="teachers__text">Со-основатель школы LVL80. Один из лучших специалистов по контекстной рекламе и аналитике на рынке Украины. Работал как с малым бизнесом, так и с крупными международными брендами.</div>
                    </div>
                </div>
            </div>
            <div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/aleksei-procenko.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/protsenko.alexey" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Алексей Проценко</div>
                        <div class="teachers__text">Руководитель отдела медиа-планирования в компании Admixer. No.1 продавца медиа-рекламы на рынке Украины. Как никто другой, знает какой баннер сработает лучше.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/julia-kolesnyk.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/julia.noname" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Юлия Колесник</div>
                        <div class="teachers__text">Более 12 лет опыта в сетевых и локальных рекламных агентствах. Copywriter в креативной компании Fedoriv, ранее Senior copywriter в BBDO Ukraine. Клиенты: Toyota, Lexus, MARS, Visa, PEPSI, Nemiroff, Libresse, Samsung и т.д.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/dmitry-kustov.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/dmytro.kustov" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Дмитрий Кустов</div>
                        <div class="teachers__text">Специалист по mobile-маркетингу. Более 10 лет опыта на digital-рынке, и более половины из них - в сфере mobile. Опыт запуска стартапов на рынки США и Европы. Активный спикер на профильных мероприятиях во многих странах.</div>
                    </div>
                </div>
            </div>
            <div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="//lvl80.pro/wp-content/themes/lvl80/dist/images/temp/teachers/anton-efimov.jpg" alt="" class="teachers__image-i">
                        </div>
                        <a href="https://www.facebook.com/anton.efimov.16" target="_blank" class="teachers__social">
                            <img src="http://lvl80.pro/wp-content/themes/lvl80/dist/images/fb-icon.svg" alt="" width="10" height="18" class="teachers__social-image">
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Антон Ефимов</div>
                        <div class="teachers__text">Более 6 лет опыта в управлении проектов. Работал в Crytek и Frag Lab. Управлял командами в 80+ человек.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/nikita-yabloko.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/appleseater" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Никита Яблоко</div>
                        <div class="teachers__text">Social Media Manager в YODEZEEN. Опыт работы: 3 года в SMM. Преподавательский стаж - 1 год. Клиенты: Freshline, Nebbia, YODEZEEN, Creamoire, Merida, Neonail, Хуторок.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/julia-davidenko.png') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/j.davydenko" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Юлия Давиденко</div>
                        <div class="teachers__text">Специалист по таргетированной рекламе в LVL80. FB-маркетолог. 4 года практики в интернет-маркетинге. Запуск рекламных кампаний на Украину, Россию, США, Европу, пространство СНГ. То что вы оказались на этом сайте - это тоже ее работа :)</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/protsenko-alexander.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/protsenko.graphics" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Александр Проценко</div>
                        <div class="teachers__text">Основатель студии веб-дизайна Dvoika, куратор курсов веб-дизайн и граф-дизайн в prjctr.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="//lvl80.pro/wp-content/uploads/2018/02/zhenya-dubrova-aliksyuk.jpg" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/Zhenya.Aliksyuk" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Евгения Дуброва-Аликсюк</div>
                        <div class="teachers__text">Специалист по контекстной рекламе. Более 5 лет опыта на разных типах проектов. Автор популярного канала в Telegram о PPC и веб-аналитике, лектор на профильных мероприятиях (в т.ч. и мероприятиях Google).</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/yuri-ostrovsky.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/yuriy.ostrovskiy.98" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Юрий Островский</div>
                        <div class="teachers__text">Маркетолог в GameDev компании Elyland. Основатель агентства "Ремаркетинг Украина". Выпускник программы mini MBA от Google (Google Elevator) в 2017. Более 7 лет опыта запуска эффективных рекламных кампаний.</div>
                    </div>
                </div>
            </div>
			<div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="//lvl80.pro/wp-content/uploads/2018/01/dima-tonkih.png" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/dmytro.tonkikh" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Дмитрий Тонких</div>
                                    <div class="teachers__text">Специалист по контекстной рекламе в компании ЛУН.ua. Более 5 лет практики с ТОП-рекламодателями Украины в сфере e-commerce. Один из лучших скриптовиков в Украине. Автор многих скриптов и канала в telegram об AdWords скриптах
</div>
                                </div>
                            </div>
                        </div>
						<div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="//lvl80.pro/wp-content/uploads/2018/02/dmytro-melinyshyn.jpg" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/endemionus" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Дмитрий Мелинишин</div>
                                    <div class="teachers__text">Performance Media Head в агентстве Performics группы Publicis One Ukraine. Более 10 лет опыта работы в сфере интернет-рекламы. Работает с ТОПовыми международными и локальными брендами в рамках комплексных стратегий.
</div>
                                </div>
                            </div>
                        </div>
			<div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/anton-peretyaka.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/anton.peretyaka" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Антон Перетяка</div>
                                    <div class="teachers__text">Один из лучших экспертов по таргетированной рекламе в Украине. В портфолио более 200 кейсов по продвижению в Facebook для клиентов из Украины, России, Казахстана, Испании и Китая.</div>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
</div>
