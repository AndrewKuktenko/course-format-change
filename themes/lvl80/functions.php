<?php
ini_set( 'display_errors', 1 );
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
	'lib/assets.php',    // Scripts and stylesheets
	'lib/extras.php',    // Custom functions
	'lib/setup.php',     // Theme setup
	'lib/titles.php',    // Page titles
	'lib/wrapper.php',   // Theme wrapper class
	'lib/customizer.php', // Theme customizer

	'lib/custom-post-types.php',
	'lib/amo.php',

	'lib/process_forms.php',
];

require_once get_template_directory() . '/vendor/autoload.php';

foreach ( $sage_includes as $file ) {
	if ( ! $filepath = locate_template( $file ) ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'sage' ), $file ), E_USER_ERROR );
	}

	require_once $filepath;
}
unset( $file, $filepath );

remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'embed_head', 'print_emoji_detection_script' );
remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'qtranxf_wp_head' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

/**
 * Automatically move JavaScript code to page footer, speeding up page loading time.
 */
remove_action( 'wp_head', 'wp_print_scripts' );
remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
add_action( 'wp_footer', 'wp_print_scripts', 5 );
add_action( 'wp_footer', 'wp_print_head_scripts', 5 );

function fondy_payment_process( $order_id, $amount, $desc ) {

	include get_template_directory() . '/lib/fondy/autoload.php';
	define( 'IPSP_GATEWAY', 'api.fondy.eu' );

	define( 'MERCHANT_ID', getenv( 'MERCHANT_ID' ) );
	define( 'MERCHANT_PASSWORD', getenv( 'MERCHANT_PASSWORD' ) );

	$client = new Ipsp_Client( MERCHANT_ID, MERCHANT_PASSWORD, IPSP_GATEWAY );
	$ipsp   = new Ipsp_Api( $client );

	switch ( qtranxf_getLanguage() ) {
		case "ru":
			$currency = $ipsp::RUB;
			break;
		case "de":
			$currency = $ipsp::EUR;
			break;
		case "en":
			$currency = $ipsp::USD;
			break;
		default:
			$currency = $ipsp::UAH;
	}

	$params = array(
		'order_id'     => ( $order_id + time() ),
		'order_desc'   => $desc,
		'currency'     => $currency,
		'amount'       => $amount,
		'response_url' => admin_url( "admin-post.php?action=callback&order_id=$order_id" )
	);

	$data = $ipsp->call( 'checkout', $params )->getResponse();

	$data = (array) $data;
	$data = array_pop( $data );

	if ( $data['response_status'] === 'success' ) {
		return $data;
	}

	wp_mail( 'serguk89@gmail.com', 'Fondy error on ' . $_SERVER['HTTP_HOST'], print_r( $data, true ) );
	return false;
}

add_action( 'admin_post_nopriv_callback', 'pay_callback' );
add_action( 'admin_post_callback', 'pay_callback' );
function pay_callback() {
	$amo = get_amo_instance();
	$id  = (int) $_GET['order_id'];

	$amo_lead    = $amo->lead;
	$amo_contact = $amo->contact;

	if ( ( $lead = $amo_lead->apiList( [ 'id' => $id ] ) ) && count( $lead ) === 1 ) {
		$lead = $lead[0];

		$amo_lead              = $amo->lead;
		$amo_lead['status_id'] = 17605468;
		$amo_lead->addCustomField( 456223, true );
		$amo_lead->apiUpdate( $id, 'now' );

		$custom_fields = [];
		foreach ( $lead['custom_fields'] as $cf ) {
			if ( ! isset( $cf['values'][0]['value'] ) ) {
				continue;
			}

			$custom_fields[ $cf['id'] ] = $cf['values'][0]['value'];
		}


		$contact = $amo_contact->apiList( [ 'id' => $lead['main_contact_id'] ] )[0];

		$msg = [];

		$msg[] = $lead['name'];
		$msg[] = 'Клиент: ' . $contact['name'];
		$msg[] = 'Сумма сделки: ' . $lead['price'] . $custom_fields[412837];
		foreach ( $contact['custom_fields'] as $field ) {
			$msg[]                     = $field['name'] . ': ' . $field['values'][0]['value'];
			$contact[ $field['name'] ] = $field['values'][0]['value'];
		}

		global $q_config;
		$q_config['language'] = $custom_fields[461657];

		$mailer          = mailer_instance();
		$mailer_group_id = get_field( 'id_покупка_' . $custom_fields[461655], $custom_fields[461661] );

		$mailer->post( "lists/$mailer_group_id/members", [
			'email_address' => $contact['Email'],
			'status'        => 'subscribed',
		] );

		wp_mail( 'o.bokman@lvl80.pro', $lead['name'], join( "\n", $msg ) );

	}

	wp_redirect( get_the_permalink( 75 ) );
}

add_filter( 'site_transient_update_plugins', '__return_false' );

add_filter( 'main_tag_class', function ( $class ) {
	$class = [];

	return join( ' ', $class );
} );

add_filter( 'get_the_excerpt', function ( $content ) {

	return wp_trim_words( $content, 16 );
} );

add_filter( 'body_class', function ( $class ) {
	if ( is_page( 'contacts' ) && $pos = array_search( 'contacts', $class ) ) {
		unset( $class[ $pos ] );
	}

	if ( $pos = array_search( 'adwords', $class ) ) {
		unset( $class[ $pos ] );
	}

	return $class;
} );

add_filter( 'wp_calculate_image_srcset', '__return_false' );

// include custom jQuery
function include_custom_jquery() {

	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, true );

}

add_action( 'init', 'include_custom_jquery', 1001 );


add_action( 'init', 'do_rewrite' );
function do_rewrite() {
	add_rewrite_rule( '(checkout)/([a-f0-9]{32})/?([^/]+)?$', 'index.php?pagename=$matches[1]&hash=$matches[2]&part=$matches[3]' );

	add_filter( 'query_vars', function ( $vars ) {
		$vars[] = 'hash';
		$vars[] = 'part';

		return $vars;
	} );

	flush_rewrite_rules( true );
}

function custom_excerpt_length() {
	return 22;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
add_filter( 'jpeg_quality', function () {
	return 90;
} );

add_filter( 'wpcf7_form_elements', function ( $content ) {
	$content = preg_replace( '/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content );

	return $content;
} );

add_filter( 'nav_menu_css_class', function ( $classes, $item ) {

	if ( $item->type == 'custom' && $pos = array_search( 'current-menu-item', $classes ) ) {

		unset( $classes[ $pos ] );
	}

	return $classes;
}, 10, 2 );

function cleanup_default_rewrite_rules( $rules ) {
	foreach ( $rules as $regex => $query ) {
		if ( strpos( $regex, 'attachment' ) || strpos( $query, 'attachment' ) ) {
			unset( $rules[ $regex ] );
		}
	}

	return $rules;
}

add_filter( 'rewrite_rules_array', 'cleanup_default_rewrite_rules' );
function cleanup_attachment_link( $link ) {
	return;
}

add_filter( 'attachment_link', 'cleanup_attachment_link' );

add_action( 'template_redirect', function () {
	if ( is_attachment() ) {
		global $post;
		if ( $post->post_parent ) {
			wp_redirect( get_permalink( $post->post_parent ), 301 );
		} else {
			wp_redirect( home_url(), 301 );
		}

		die;
	} if (is_page() && get_post()->post_name === 'checkout') {

		if ((!$md5 = get_query_var('hash')) ||
		    !$post = get_posts(['post_type' => 'payment', 'meta_key' => 'payment_hash', 'meta_value' => $md5])) {
			die(wp_redirect(home_url()));
		}

		global $originalCourse, $price, $url;
		$originalCourse = get_post(get_post_meta($post[0]->ID, 'payment_postID', true));
		if (!$originalCourse) {
			die(wp_redirect(home_url()));
		}

		$type = get_post_meta($post[0]->ID, 'payment_type', true);

		$price = [
			'full' => get_field("цена_{$type}_full", $originalCourse->ID),
			'old'  => get_field("цена_{$type}_full_old", $originalCourse->ID),
            'part' => get_field("цена_$type", $originalCourse->ID, false),
		];

		$url = [
			'full' => home_url("/checkout/$md5/full/"),
			'part' => home_url("/checkout/$md5/part/")
		];

		if (($part = get_query_var('part'))) {
			if (!in_array($part, ['part', 'full'])) {
				die(wp_redirect(home_url()));
			}

			$amount = (int)$price[$part] * 100;
			$leadID = get_post_meta($post[0]->ID, 'payment_leadID', true);

			$desc = '[Полная оплата] Покупка курса - ' . get_the_title( $originalCourse ) . ' (' . $type . ')';

			if ($response = fondy_payment_process($leadID, $amount, $desc)) {
				die(wp_redirect($response['checkout_url']));
			}

				die(wp_redirect(home_url()));

		}

	}

} );

function mailer_instance() {
	static $mailer_inst;

	if ( ! $mailer_inst ) {
		$mailer_inst = new \DrewM\MailChimp\MailChimp( getenv( 'MailChimp_key' ) );
	}

	return $mailer_inst;
}

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings'
	) );

}

add_action( 'wp_ajax_get_posts', 'get_ajax_blog_posts' );
add_action( 'wp_ajax_nopriv_get_posts', 'get_ajax_blog_posts' );
function get_ajax_blog_posts() {

	if ( empty( $_POST['ids'] ) ) {
		die;
	}

	//retrieve if isset search var
	preg_match( '@s=([^&]+)@', $_SERVER['HTTP_REFERER'], $search_arg );

	ob_start();
	$args  = [
		'post__not_in' => $_POST['ids'],
		'cat'          => $_POST['cat'] ?? null,
		's'            => $search_arg[1] ?? null,
	];
	$query = new WP_Query( $args );

	while ( $query->have_posts() ) {
		$query->the_post();

		get_template_part( 'templates/post', 'item' );
	}

	wp_send_json_success( [
		'html'      => ob_get_clean(),
		'show_more' => $query->get( 'posts_per_page' ) === $query->post_count
	] );
	die;
}

add_filter( 'wpseo_breadcrumb_single_link', 'ss_breadcrumb_single_link' );
function ss_breadcrumb_single_link( $link_output ) {
	return str_replace( '<a', '<a class="breadcrumbs__link"', $link_output );
}

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'my_new_gallery_function' );
function my_new_gallery_function( $atts ) {

	global $post;
	$pid     = $post->ID;
	$gallery = "";

	if ( empty( $pid ) ) {
		$pid = $post['ID'];
	}

	if ( ! empty( $atts['ids'] ) ) {
		$atts['orderby'] = 'post__in';
		$atts['include'] = $atts['ids'];
	}

	extract( shortcode_atts( array(
		'orderby'    => 'menu_order ASC, ID ASC',
		'include'    => '',
		'id'         => $pid,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'large',
		'link'       => ''
	), $atts ) );

	$args = array(
		'post_type'      => 'attachment',
		'post_status'    => 'inherit',
		'post_mime_type' => 'image',
		'orderby'        => $orderby
	);

	if ( ! empty( $include ) ) {
		$args['include'] = $include;
	} else {
		$args['post_parent'] = $id;
		$args['numberposts'] = - 1;
	}

	if ( $args['include'] == "" ) {
		$args['orderby'] = 'date';
		$args['order']   = 'asc';
	}

	$images = get_posts( $args );

	if ( ! $images ) {
		return $gallery;
	}

	foreach ( $images as $image ) {
		$gallery .= sprintf( '<div class="image-slider__item">%s</div>', wp_get_attachment_image( $image->ID, $size ) );
	}

	return sprintf( '<div class="image-slider">%s</div>', $gallery );
}