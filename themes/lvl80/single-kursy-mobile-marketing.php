<? use Roots\Sage\Assets;?>
    <div style="background-image:url(<?= Assets\asset_path('images/temp/mobile-marketing-promo-cover.jpg') ?>;" class="promo promo_dark-orange promo_simple">
        <div class="container">
            <div class="promo__inner">
                <div class="promo__content">
                    <h2 class="h1 promo__title">Mobile marketing. PRO</h2>
                    <div class="promo__text">Этот курс даст понимание как построить продвижение мобильного приложения с нуля. Вы узнаете о стратегиях продвижения различных типов мобильных приложений. Вы научитесь выбирать подход в рекламе приложения в зависимости от способа монетизации приложения.</div>
	                <? get_template_part('templates/kursy', 'promo__btns') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="info-box">
        <div class="container">
            <div class="info-box__wrap">
                <div class="info-box__content">
                    <div class="info-box__text">
                        <p>Этот курс поможет вам в задаче, как сделать успешным продукт ориентированный преимущественно на пользователей мобильных устройств. С чего начать, в каком порядке действовать, какие ошибки не стоит совершать. Этот базовый курс выступает путеводителем
                            по основным направлениям маркетинга мобильного продукта.</p>
                    </div><a href="#section-schedule" class="btn btn-dark-orange info-box__button scroll-link">Просмотреть программу курса</a>
                </div>
                <div class="info-box__aside">
                    <div class="info-box__image">
                        <img src="<?= Assets\asset_path('images/temp/mobile-marketing-image.jpg') ?>" alt="" class="info-box__image-i">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="section-cources" class="classes classes_offset-bottom-simple">
        <div class="container">
            <div class="classes__list">
                <div class="classes__list-item"><a data-toggle="collapse" href="#class-mobile-marketing" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-8.svg') ?>" alt="" width="87" height="87" class="classes__item-image-i"></span><span class="classes__item-content"><span class="classes__item-title">Mobile marketing. PRO</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                    <div id="class-mobile-marketing" class="classes__content collapse in">
                        <hr class="no-offset-top">
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Данный курс идеально подойдет:</div>
                        </div>
                        <div class="info-list">
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-11.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title">Разработчикам мобильных приложений</div>
                                    <div class="info-list__text">которые планируют продвигать свой продукт самостоятельно</div>
                                </div>
                            </div>
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-5.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title">Начинающим маркетологам</div>
                                    <div class="info-list__text">которые хотят ориентироваться в мобильном маркетинге</div>
                                </div>
                            </div>
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title">Маркетологам</div>
                                    <div class="info-list__text">которые хотят уйти в мобильный маркетинг из веба</div>
                                </div>
                            </div>
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-1.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title">Владельцам приложений</div>
                                    <div class="info-list__text">для понимания принципов мобильного маркетинга</div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Длительность занятий</div>
                            <div class="classes__content-text">Курс состоит из <strong>9 занятий</strong>, каждую неделю проходит по <strong>2 занятия</strong>
                                <br><span class="color-brand">(понедельник и среда в 19:30, GMT +2)</span>
                            </div>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                            <div class="classes__content-text">Курс ориентирован на то, чтобы дать основы понимания комплексного продвижения мобильных приложений (включая аспекты ASO, фичеринга, трекинга, анализа аудитории, етс), а не сосредоточен исключительно на закупке трафика. Упор на практические навыки.</div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-dark-orange scroll-link">Оформить заявку на курс</a>
                        </div>
                        <hr>
                        <div id="section-schedule" class="classes__content-wrap">
                            <div class="classes__content-title">Что научитесь делать:</div>
                        </div>
                        <div class="schedule-list schedule-list_col-1 schedule-list_dark-orange">
                            <div class="schedule-list__column">
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Поймете рынок мобильного маркетинга</div>
                                    <div class="schedule-list__text">Терминология, отличия от веба, основные типы задач, типы приложений.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Оценивание приложения</div>
                                    <div class="schedule-list__text">Базовые способы оценки, soft-launch, внешние сервисы.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Составление стратегии продвижения</div>
                                    <div class="schedule-list__text">Воронки поведения, методы продвижения, типы каналов и стратегий.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Подходы к разным каналам продвижения</div>
                                    <div class="schedule-list__text">Составление плана, оценка CPI, специализированные платформы.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Мошеннические схемы</div>
                                    <div class="schedule-list__text">Методы, которые нужно избегать, способы обхода.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Работа с рекламным кабинетом Facebook</div>
                                    <div class="schedule-list__text">Составление рекламных кампаний, оптимизация под CPI, Facebook Audience Network.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Работа с рекламным кабинетом Adwords</div>
                                    <div class="schedule-list__text">Universal кампании, оптимизация под CPI, работа с тарегтингами.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Работа с бесплатным трафиком</div>
                                    <div class="schedule-list__text">Источники, ASO, фичеринг, контентное продвижение.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Тестирование и анализ приложений</div>
                                    <div class="schedule-list__text">Системы аналитики, проведение тестов, UX анализ приложений.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Монетизация приложений</div>
                                    <div class="schedule-list__text">Виды монетизации, сервисы монетизации, разный подход к продвижению.</div>
                                </div>
                            </div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-dark-orange scroll-link">Записаться на курс</a>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Кто ведет курс</div>
                            <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, к их компетенции и опыту, поэтому выбрали для вас лучших из лучших. Этот курс ведут только практики!</div>
                        </div>
                        <div class="teachers">
                            <div class="teachers__item">
                                <div class="teachers__item-inner">
                                    <div class="teachers__aside">
                                        <div class="teachers__image">
                                            <img src="<?= Assets\asset_path('images/temp/teachers/dmitry-kustov.jpg') ?>" alt="" class="teachers__image-i">
                                        </div>
                                        <a href="https://www.facebook.com/dmytro.kustov" target="_blank" class="teachers__social">
                                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image">
                                        </a>
                                    </div>
                                    <div class="teachers__content">
                                        <div class="teachers__title">Дмитрий Кустов</div>
                                        <div class="teachers__text">Специалист по mobile-маркетингу. Более 10 лет опыта на digital-рынке, и более половины из них - в сфере mobile. Опыт запуска стартапов на рынки США и Европы. Активный спикер на профильных мероприятиях во многих странах.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="teachers__item">
                                <div class="teachers__item-inner">
                                    <div class="teachers__aside">
                                        <div class="teachers__image">
                                            <img src="<?= Assets\asset_path('images/temp/teachers/yuri-ostrovsky.jpg') ?>" alt="" class="teachers__image-i">
                                        </div>
                                        <a href="https://www.facebook.com/yuriy.ostrovskiy.98" target="_blank" class="teachers__social">
                                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image">
                                        </a>
                                    </div>
                                    <div class="teachers__content">
                                        <div class="teachers__title">Юрий Островский</div>
                                        <div class="teachers__text">Маркетолог в GameDev компании Elyland. Основатель агентства "Ремаркетинг Украина". Выпускник программы mini MBA от Google (Google Elevator) в 2017. Более 7 лет опыта запуска эффективных рекламных кампаний.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="teachers__item">
                                <div class="teachers__item-inner">
                                    <div class="teachers__aside">
                                        <div class="teachers__image">
                                            <img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i">
                                        </div>
                                        <a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
                                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image">
                                        </a>
                                    </div>
                                    <div class="teachers__content">
                                        <div class="teachers__title">Виктор Локотков</div>
                                        <div class="teachers__text">Со-основатель школы LVL80. Более 10 лет опыта в интернет маркетинге. Работал руководителем отдела маркетинга в Genius Marketing, где Facebook является основным источником привлечения клиентов.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-dark-orange scroll-link">Оформить заявку на курс</a>
                        </div>
                        <hr>
                        <div id="section-price" class="classes__content-wrap">
                            <div class="classes__content-title">Стоимость курса</div>
                            <div class="classes__content-text">Курс длится 1 месяц. Состоит из 9 лекций. Проходит 2 раза в неделю. Количество мест ограничено.</div>
                        </div>
                        <? get_template_part('templates/kursy', 'price') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? get_template_part('templates/kursy', 'list') ?>
    <div class="contacts contacts_dark-orange">
        <div class="container">
            <div class="contacts__inner">
                <div class="contacts__title">Еще думаете?</div>
                <div class="contacts__text">Напишите нам и мы развеем все сомнения:)</div>
                <div class="contacts__image">
                    <img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
                </div>
                <div class="contacts__controls">
                    <div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                    </div>
                    <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
