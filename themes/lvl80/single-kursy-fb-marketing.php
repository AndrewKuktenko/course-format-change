<? use Roots\Sage\Assets;?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/fb-promo-cover.jpg') ?>');" class="promo promo_blue promo_simple">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Facebook marketing. PRO</h2>
                <div class="promo__text">Профессии «Facebook маркетолог» еще нет, а потребность в таких специалистах уже есть. Особенно в связи с последними изменениями Facebook, которые вытесняют брендовый и медиа SMM. Стоимость рекламы растет. Управлять кампаниями нужно, как никогда, грамотно. Мы научим тому как это правильно делать и как выжимать из Facebook максимум!</div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Facebook уже давно не просто социальная сеть. Это интернет в интернете. Рынку нужны Facebook маркетологи, которые умеют грамотно вести SMM, настраивать рекламные кампании, писать продающие объявления, создавать креативы, снимать и монтировать видео. </p>
                    <p>На курсе <strong>Facebook marketing. PRO</strong> вы научитесь комплексно использовать все инструменты социальной сети. Вы узнаете, что SMM, таргетированная реклама и чатботы – это далеко не все возможности Facebook.</p>
                </div><a href="#section-schedule" class="btn btn-blue info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/fb-marketing-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-fb-marketing" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-5.svg') ?>" alt="" width="89" height="89" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Facebook marketing. PRO</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                    id="class-fb-marketing" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list info-list_simple">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Продвинутым маркетологам</div>
                                <div class="info-list__text">которые хотят правильно и в полной мере использовать продвижение в Facebook.</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-1.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Профильным специалистам</div>
                                <div class="info-list__text">(SMMщикам, таргетологам) которые хотят расширить свое представление о Facebook и подходить к нему комплексно.</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-11.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Владельцам
                                    <br>бизнеса</div>
                                <div class="info-list__text">которые уже привлекают клиентов из Facebook, но хотят понимать все возможности.</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс состоит из <strong>13 занятий</strong>, каждую неделю проходит по 2 занятия
                            <br><span class="color-brand">(понедельник и среда в 19:30, GMT+2)</span>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                        <div class="classes__content-text">Это уникальный курс. Аналогов на рынке нет. Мы готовим специалистов, о которых все мечтают, но не знают, где искать. Этот курс — баланс практики и теории. Готовьтесь к тому, что вам придется тратить в среднем по 4 часа на практическую домашнюю работу после каждого занятия. Только выполняя домашние задания, вы получите максимальный результат!
</div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-blue scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_blue schedule-list_col-1">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Стратегия продвижения в Facebook</div>
                                <div class="schedule-list__text">Разберемся, какие есть возможности продвижения в Facebook и как их использовать стратегически.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">SMM-стратегия</div>
                                <div class="schedule-list__text">Узнаем, какие есть SMM стратегии, как правильно составить контент-план и что поможет быстрее развить страницу в Facebook.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работа с инструментами SMM</div>
                                <div class="schedule-list__text">Разберемся со всеми SMM инструментами, которые нам предлагает сам Facebook, а также изучим основные сторонние инструменты SMM-специалиста.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Business Manager</div>
                                <div class="schedule-list__text">Научимся грамотно создавать учетные записи в Facebook, подключать коллег и агентства к работе, анализировать их работу.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разбор рекламного аккаунта Facebook</div>
                                <div class="schedule-list__text">Разберемся во всем функционале рекламного кабинета. Узнаем для чего нужны аудитории, пиксели, отслеживание конверсий, и как все это использовать.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Реклама в Instagram</div>
                                <div class="schedule-list__text">Разберемся с особенностями продвижения в Instagram.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Создание интернет-магазина в Facebook</div>
                                <div class="schedule-list__text">Узнаем, как создать интернет-магазин в Facebook и как продвигать товары с вашего сайта в Facebook.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Facebook Messenger</div>
                                <div class="schedule-list__text">Научимся создавать чат-бота, вписывать клиентов в воронку, проводить рассылку в Messenger.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Основы копирайтинга</div>
                                <div class="schedule-list__text">Научимся писать такие посты в SMM, чтоб люди хотели их лайкать, шейрить, комментить и покупать.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Основы видеопродакшена</div>
                                <div class="schedule-list__text">Узнаем, как правильно снимать ролики, даже при помощи мобильного телефона, и проводить базовый монтаж видео.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Создание креативов</div>
                                <div class="schedule-list__text">Научимся делать крутые баннера для рекламы и постов в Facebook без помощи дизайнеров.</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-blue scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень тщательно выбираем преподавателей, оценивая их компетенцию и опыт. Этот курс ведут только практики!</div>
                    </div>
                    <div class="teachers">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Виктор Локотков</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Более 10 лет опыта в интернет маркетинге. Работал руководителем отдела маркетинга в Genius Marketing, где Facebook является основным источником привлечения клиентов.</div>
                                </div>
                            </div>
                        </div>

                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-davidenko.png') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/j.davydenko" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Давиденко</div>
                                    <div class="teachers__text">Специалист по таргетированной рекламе в LVL80. FB-маркетолог. 4 года практики в интернет-маркетинге. Запуск рекламных кампаний на Украину, Россию, США, Европу, пространство СНГ. То что вы оказались на этом сайте - это тоже ее работа :)</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-kolesnyk.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/julia.noname" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Колесник</div>
                                    <div class="teachers__text">Более 12 лет опыта в сетевых и локальных рекламных агентствах. Copywriter в креативной компании Fedoriv, ранее Senior copywriter в BBDO Ukraine. Клиенты: Toyota, Lexus, MARS, Visa, PEPSI, Nemiroff, Libresse, Samsung.</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/rymar-daria.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/rymardariay" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Дарья Рымарь</div>
                                    <div class="teachers__text">SMM-специалист с опытом работы в онлайн маркетинге более 5ти лет.</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/protsenko-alexander.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/protsenko.graphics" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Александр Проценко</div>
                                    <div class="teachers__text">Основатель студии веб-дизайна Dvoika, куратор курсов веб-дизайн и граф-дизайн в prjctr.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-blue scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 1,5 месяца. Состоит из 13 лекций. Проходит 2 раза в неделю. Количество мест ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts contacts_blue">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите мне и я развею все сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lokotkov-photo.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="http://m.me/olwlo" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>