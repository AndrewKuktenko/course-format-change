<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>
    <div class="page-wrap__aside">
		<?php dynamic_sidebar( 'right-sidebar' ); ?>
    </div>
<?php endif; ?>