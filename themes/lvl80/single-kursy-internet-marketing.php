<? use Roots\Sage\Assets;?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/im-promo-cover.jpg') ?>');" class="promo promo_pink promo_simple">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Основы интернет-маркетинга</h2>
                <div class="promo__text">Этот курс даст ответы на самые базовые вопросы и позволит двигаться дальше в выбранном направлении. Вы научитесь мыслить стратегически и пользоваться самыми эффективными инструментами современного маркетолога.</div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Этот курс создан для того, чтобы дать бизнесу маркетологов дела — специалистов, которые видят результаты своей работы в финансовых показателях компании. Маркетологов, которые нужны современным клиентам и рынку.</p>
                    <p>На курсе <strong>Основы интернет-маркетинга</strong> научитесь пользоваться самыми распространенными инструментами маркетолога и опираться в своих решениях на объективные данные.</p>
                </div><a href="#section-schedule" class="btn info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/internet-marketing-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-internet-marketing" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-1.svg') ?>" alt="" width="94" height="94" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Основы интернет-маркетинга</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                        id="class-internet-marketing" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-1.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Начинающим маркетологам</div>
                                <div class="info-list__text">которые хотят перешагнуть на новую ступень</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-2.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Профильным специалистам</div>
                                <div class="info-list__text">(SMMщикам, SEOшникам, PRщикам), которые хотят расширить свое представление об онлайн-маркетинге</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Офис-менеджерам</div>
                                <div class="info-list__text">которым надоело быть офис-менеджерами</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-4.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Студентам</div>
                                <div class="info-list__text">которые хотят освоить новую профессию</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс длится <strong>3 месяца</strong>, каждую неделю проходит по 2 занятия
                            <br><span class="color-brand">(вторник и четверг в 19:30, GMT+2)</span>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                        <div class="classes__content-text">Этот курс — баланс практики и теории. Готовьтесь к тому, что вам придется тратить в среднем по 8 часов на практическую домашнюю работу после каждого занятия. Только выполняя домашние задания, вы получите максимальный результат!</div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Целевая аудитория</div>
                                <div class="schedule-list__text">Поймем как правильно выделять целевую аудиторию, что такое аватар пользователя и как это использовать в маркетинге</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">УТП</div>
                                <div class="schedule-list__text">Научимся правильно составлять УТП, выделять сильные стороны продукта и описывать их в выгодном свете</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Анализ конкурентов</div>
                                <div class="schedule-list__text">Узнаем об основных метриках и инструментах анализа конкурентов</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Поисковая оптимизация</div>
                                <div class="schedule-list__text">Научимся составлять семантическое ядро, проводить аудит внутренней оптимизации сайта и выстраивать внешнюю оптимизацию</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Контекстная реклама</div>
                                <div class="schedule-list__text">Подробно разберемся в Google Adwords, узнаем что можно, а что нельзя делать в контекстной рекламе. Настроим первую рекламную кампанию.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Медийная реклама</div>
                                <div class="schedule-list__text">Узнаем, что медийка жива, цветет и пахнет. Как правильно планировать бюджет медийной кампании и какого эффекта ожидать.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">E-mail Маркетинг</div>
                                <div class="schedule-list__text">Научимся выстраивать цепочку взаимодействия с клиентом через почтовый ящик, создадим первое письмо, узнаем какие есть типы писем и для чего он нужны.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Ремаркетинг</div>
                                <div class="schedule-list__text">научимся настраивать и запускать ремаркетинговые кампании в Google и Facebook.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">SMM</div>
                                <div class="schedule-list__text">Научимся выстраивать SMM- и контент-стратегию. Поймем как правильно создавать контент-план и какие посты заходят лучше всего.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Таргетированная реклама в Facebook</div>
                                <div class="schedule-list__text">Научимся создавать аудитории, настраивать конверсии, устанавливать пиксель и запускать рекламные кампании в Facebook.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">PR</div>
                                <div class="schedule-list__text">Поймем, что PR не настолько сложен как его рисуют. Научимся правильно питчить журналистов и находить с ними контакт.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Копирайтинг</div>
                                <div class="schedule-list__text">Научимся писать продающие тексты. Как короткие, так и длинные истории.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Маркетинг в мессенджерах</div>
                                <div class="schedule-list__text">Узнаем как правильно вписывать клиентов в мессенджер, что им там отправлять и как создавать чат-ботов.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Бюджетирование</div>
                                <div class="schedule-list__text">Научимся правильно составлять бюджет, как оценивать эффективность рекламных каналов.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Планирование</div>
                                <div class="schedule-list__text">Научимся планировать рекламные кампании.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Маркетинговая стратегия</div>
                                <div class="schedule-list__text">Создадим 2 настоящие, полноценные маркетинговые стратегии для реальных клиентов.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Реклама мобильной версии сайта</div>
                                <div class="schedule-list__text">Узнаем, чем отличается реклама мобильной версии сайта от десктопной и какие особенности важно учитывать.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Реклама мобильных приложений</div>
                                <div class="schedule-list__text">Поймем, чем отличается реклама мобильной версии сайта от рекламы мобильного приложения и как правильно оценивать эффективность рекламных кампаний в мобильных приложениях.</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, к их компетенции и опыту, поэтому подобрали для вас лучших из лучших. Этот курс ведут только практики!</div>
                    </div>
                    <div class="teachers">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Виктор Локотков</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Более 10 лет опыта в интернет маркетинге. Работал руководителем отдела маркетинга в Genius Marketing, запустил сервис Eda.ua на рынке Украины, ментор в Google Launchpad. Спикер на более чем 100 конференциях по бизнесу и маркетингу.</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/anton-lipsky.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/lipskiy.anton/" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Антон Липский</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Один из лучших специалистов по контекстной рекламе и аналитике на рынке Украины. Работал как с малым бизнесом, так и с крупными международными брендами, среди которых были L’Oreal, Samsung, Nestle и другие</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/aleksei-procenko.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/protsenko.alexey" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Алексей Проценко</div>
                                    <div class="teachers__text">Руководитель отдела медиа-планирования в компании Admixer. No.1 продавца медиа-рекламы на рынке Украины. Как никто другой, знает какой баннер сработает лучше.</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-kolesnyk.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/julia.noname" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Колесник</div>
                                    <div class="teachers__text">Former Senior copywriter в BBDO Ukraine. Опыт работы: 12 лет в сетевых и локальных рекламных агентствах. Клиенты: Toyota, Lexus, MARS, Visa, PEPSI, Nemiroff, Libresse, Samsung и т.д.</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 3 месяца. Состоит из 27 лекций. Проходит 2 раза в неделю. Количество мест ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите мне и я развею все сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lokotkov-photo.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="http://m.me/olwlo" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>