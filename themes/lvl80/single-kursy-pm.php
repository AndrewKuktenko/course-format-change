<? use Roots\Sage\Assets;?>
    <div style="background-image:url('<?= Assets\asset_path('images/temp/pm-promo-cover.jpg') ?>');" class="promo promo_light-blue promo_simple">
        <div class="container">
            <div class="promo__inner">
                <div class="promo__content">
                    <h2 class="h1 promo__title">Project Management. Basic</h2>
                    <div class="promo__text">Project Manager – это уже не просто роль в проекте. Это управленец, лидер, друг, вдохновитель. На курсе мы научим вас тому, как правильно выстраивать работу команды, контролировать ее, выводить на совершенно новый уровень.</div>
	                <? get_template_part('templates/kursy', 'promo__btns') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="info-box">
        <div class="container">
            <div class="info-box__wrap">
                <div class="info-box__content">
                    <div class="info-box__text">
                        <p>Сейчас хороший Project-manager должен разбираться не только в Kanban, Waterfall, Gantt и Scrum. Первое и главное, что должен знать и уметь PM – это работа с людьми. Основа нашего курса – это People Management и управление командой.</p>
						<p>На курсе мы разберем весь набор инструментов Project-manager'а. Поделимся самыми полезными знаниями, для приготовления “коктейля” процессов и улучшений, который необходим в вашем проекте.</p>
                    </div><a href="#section-schedule" class="btn btn-light-blue info-box__button scroll-link">Просмотреть программу курса</a>
                </div>
                <div class="info-box__aside">
                    <div class="info-box__image">
                        <img src="<?= Assets\asset_path('images/temp/pm-image.jpg') ?>" alt="" class="info-box__image-i">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="section-cources" class="classes classes_offset-bottom-simple">
        <div class="container">
            <div class="classes__list">
                <div class="classes__list-item"><a data-toggle="collapse" href="#class-pm" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-9.svg') ?>" alt="" width="89" height="89" class="classes__item-image-i"></span><span class="classes__item-content"><span class="classes__item-title">Project Management. Basic</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                    <div id="class-pm" class="classes__content collapse in">
                        <hr class="no-offset-top">
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Данный курс идеально подойдет:</div>
                        </div>
                        <div class="info-list info-list_simple">
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-5.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title" style="height: 26px;">Junior/Middle PM</div>
                                    <div class="info-list__text">которые хотят значительно расширить свои знания</div>
                                </div>
                            </div>
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title" style="height: 26px;">Team Leads</div>
                                    <div class="info-list__text">которые хотят понимать своих PM и лучше работать с командой</div>
                                </div>
                            </div>
                            <div class="info-list__item">
                                <div class="info-list__image">
                                    <img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i">
                                </div>
                                <div class="info-list__content">
                                    <div class="info-list__title" style="height: 26px;">Руководители бизнеса</div>
                                    <div class="info-list__text">которые хотят наладить процессы в своей компании</div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Длительность занятий</div>
                            <div class="classes__content-text">Курс длится <strong>2 месяца</strong>, каждую неделю проходит по <strong>2 занятия</strong>
                                <br><span class="color-brand">(вторник и четверг в 19:30, GMT+2)</span>
                            </div>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                            <div class="classes__content-text">Курс ведут только практики с опытом управления командой от 100 человек. Готовьтесь к тому, что вам придется тратить в среднем по 4 часа на практическую домашнюю работу после каждого занятия. Только выполняя домашние задания, вы получите максимальный результат!</div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-light-blue scroll-link">Оформить заявку на курс</a>
                        </div>
                        <hr>
                        <div id="section-schedule" class="classes__content-wrap">
                            <div class="classes__content-title">Что научитесь делать:</div>
                        </div>
                        <div class="schedule-list schedule-list_col-1 schedule-list_light-blue">
                            <div class="schedule-list__column">
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Agile</div>
                                    <div class="schedule-list__text">Agile manifesto: разберем что это такое. Почему стало популярным и распространенным в IT и в разработке игр в частности. Будем использовать как “призму”, через которую смотрим на процессы и управление. </div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Scrum</div>
                                    <div class="schedule-list__text">Один из эффективнейших подходов при гибкой разработке. Очень эффективен для разработки продуктов в среде с сильным “туманом войны”. Разберем этот фреймворк, его правила, инструменты, роли и артефакты.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Retrospective</div>
                                    <div class="schedule-list__text">Важная практика, направленная на Continuous Improvement.
Один из “инструментов”, который при правильном внедрении и проведении дает чертовски хорошие результаты. Как “делать” и как “не делать” из опыта.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Risks management</div>
                                    <div class="schedule-list__text">Разберем простейший инструмент для анализа рисков и построению плана по работе с ними. Очень просто, но чертовски эффективно.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Lean</div>
                                    <div class="schedule-list__text">Одна из полезнейших концепций управления, если не самая полезная. Поможет по новому посмотреть на организацию в целом. Увидеть проблемы в процессах и чинить их при помощи ряда инструментов. Lean как базис Continuous Improvement.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Critical Chain method (+Gant)</div>
                                    <div class="schedule-list__text">Метод критической цепи простой и эффективный инструмент для планирования расписания проекта и управления сроками проекта. Разберемся как работает метод, как им пользоваться на практике. Научимся определять критические и некритические задачи.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Kanban</div>
                                    <div class="schedule-list__text">Система организации производства, позволяющая реализовать концепцию “точно в срок”. Разберем что это, как и почему работает.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Visibility</div>
                                    <div class="schedule-list__text">Часть формальных или не формальных требований к ПМ – это улучшение “прозрачности” организации  и того, что в ней творится. Увеличение прозрачности полезно и необходимо как заказчикам, вашим начальникам, так и командам.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">SMART</div>
                                    <div class="schedule-list__text">Правильно ставить задачи – must-have умение для менеджера. Поэтому техника постановки задач SMART поможет в этом.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">Soft skills</div>
                                    <div class="schedule-list__text">Разберемся в том, что такое Soft Skills. Почему для Проектного Менеджера Soft Skills являются невероятно важными.</div>
                                </div>
                                <div class="schedule-list__item">
                                    <div class="schedule-list__item-icon"></div>
                                    <div class="schedule-list__title">PM’s daily routine and signal systems</div>
                                    <div class="schedule-list__text">Ежедневная рутина Проектного менеджера - какая она может быть, почему важно это понимать и как это помогает команде и организации.</div>
                                </div>
                            </div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-light-blue scroll-link">Записаться на курс</a>
                        </div>
                        <hr>
                        <div class="classes__content-wrap">
                            <div class="classes__content-title">Кто ведет курс</div>
                            <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, к их компетенции и опыту, поэтому выбрали для вас лучших из лучших. Этот курс ведут только практики!</div>
                        </div>
                        <div class="teachers teachers_centered">
                            <div class="teachers__item">
                                <div class="teachers__item-inner">
                                    <div class="teachers__aside">
                                        <div class="teachers__image">
                                            <img src="<?= Assets\asset_path('images/temp/teachers/anton-efimov.jpg') ?>" alt="" class="teachers__image-i">
                                        </div>
                                        <a href="https://www.facebook.com/anton.efimov.16" target="_blank" class="teachers__social">
                                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image">
                                        </a>
                                    </div>
                                    <div class="teachers__content">
                                        <div class="teachers__title">Антон Ефимов</div>
                                        <div class="teachers__text">Более 6 лет опыта в управлении проектов. Работал в Crytek и Frag Lab. Управлял командами в 80+ человек.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="classes__content-wrap"><a href="#section-price" class="btn btn-light-blue scroll-link">Оформить заявку на курс</a>
                        </div>
                        <hr>
                        <div id="section-price" class="classes__content-wrap">
                            <div class="classes__content-title">Стоимость курса</div>
                            <div class="classes__content-text">Курс длится 2 месяца. Состоит из 16 лекций. Проходит 2 раза в неделю. Можно выбрать онлайн или офлайн обучение. Количество мест на обоих потоках ограничено.</div>
                        </div>
                        <? get_template_part('templates/kursy', 'price') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? get_template_part('templates/kursy', 'list') ?>
    <div class="contacts contacts_light-blue">
        <div class="container">
            <div class="contacts__inner">
                <div class="contacts__title">Еще думаете?</div>
                <div class="contacts__text">Напишите нам и мы развеем все сомнения:)</div>
                <div class="contacts__image">
                    <img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
                </div>
                <div class="contacts__controls">
                    <div class="contacts__controls-item"><a href="https://m.me/lvl80" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                    </div>
                    <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
