<? use Roots\Sage\Assets;?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/web-analytics-hard-promo-cover.jpg') ?>');" class="promo promo_dark promo_simple">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Web-analytics. Hardcore</h2>
                <div class="promo__text">Аналитика — важнейший инструмент, позволяющий принять правильное решение в интернет-маркетинге. Курс создан для маркетологов, которым не хватает базовых знаний в этой области.</div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Этот курс создан для того, чтобы дать бизнесу маркетологов дела — специалистов, которые видят результаты своей работы в финансовых показателях компании. Маркетологов, которые нужны современным клиентам и рынку.</p>
                    <p>На курсе <strong>Web-analytics. Hardcore</strong> научитесь пользоваться самыми распространенными инструментами маркетолога и опираться в своих решениях на объективные данные.</p>
                </div><a href="#section-schedule" class="btn btn-black info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/web-analytics-hard-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-web-analytics-hard" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-4.svg') ?>" alt="" width="94" height="94" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Web-analytics. Hardcore</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                        id="class-web-analytics-hard" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Аналитикам</div>
                                <div class="info-list__text">которые хотят прокачать свой скилл сильнее</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Интернет-маркетологам</div>
                                <div class="info-list__text">которые уже много чего знают, но хотят еще больше понимать</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс длится <strong>1 месяц</strong>, каждую неделю проходит по 2 занятия
                            <br><span class="color-brand">(понедельник и среда в 19:30, GMT+2)</span>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                        <div class="classes__content-text">Этот курс дает развернутое понимание системы Google Analytics с упором на действительно необходимые знания. Работа на живых примерах и аккаунтах. Огромное количество практических домашних заданий, которые помогут еще лучше усвоить материал.</div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-black scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1 schedule-list_dark">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с ассоциированными конверсиями и моделями их аттрибуции</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Проводить А/Б тестирование в Google Analytics и Optimize</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с регулярными выражениями</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Импортировать данные в Google Analytics</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Поймете принципы работы Measurement Protocol</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберетесь в сквозной аналитике</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с Google TagManager на уровне DataLayer</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с базовыми скриптами в Google TagManager</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с аналитикой на базе User ID</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберетесь с импортом данных в Spreadsheets через Google Analytics Core Report API</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-black scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, к их компетенции и опыту, поэтому подобрали для вас лучших из лучших. Этот курс ведут только практики!</div>
                    </div>
                    <div class="teachers teachers_centered">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/anton-lipsky.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/lipskiy.anton/" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Антон Липский</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Один из лучших специалистов по контекстной рекламе и аналитике на рынке Украины. Работал как с малым бизнесом, так и с крупными международными брендами, среди которых были L’Oreal, Samsung, Nestle и другие</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-black scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 1 месяц. Состоит из 8 лекций. Проходит 2 раза в неделю. Количество мест ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts contacts_dark">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите мне и я развею все сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lipskiy-photo.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>