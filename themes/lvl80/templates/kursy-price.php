<? global $disable_free_places; ?>
<div class="prices-box">
    <div data-type="online" class="prices-box__item">
        <div class="prices-box__title">Онлайн обучение</div>
        <div class="prices-box__text">Отлично подойдет тем, кто не может приезжать ежедневно в Киев</div>
        <? if($price = get_field('цена_онлайн')): ?>
            <div class="prices-box__price"><?= $price ?></div>
        <? else: ?>
            <div class="prices-box__price">Бесплатно</div>
        <? endif ?>
	    <? if (!$disable_free_places): ?>
            <div class="prices-box__label">Осталось <? the_field('свободных_мест_онлайн') ?> мест</div>
	    <? endif ?>
        <a href="#order-modal" data-toggle="modal" data-price="<?= $price?:0 ?>" class="btn prices-box__button">Оформить заявку</a>
    </div>
    <? if (get_locale() == 'uk_UA'): ?>
        <div data-type="offline" class="prices-box__item">
            <div class="prices-box__title">Оффлайн обучение</div>
            <div class="prices-box__text">
                <? if (get_locale() == 'uk_UA'): ?>
                    Идеальный вариант. Уроки проходят в центре Киева. За час до каждого занятия проходит нетворкинг
                <? else: ?>
                    Это не запись. Мы ведем лекции вживую. Все участники смогут задавать вопросы в чате и обязательно будут получать ответы.
                <? endif ?>
            </div>
	        <? if($price = get_field('цена_оффлайн')): ?>
                <div class="prices-box__price"><?= $price ?></div>
	        <? else: ?>
                <div class="prices-box__price">Бесплатно</div>
	        <? endif ?>
            <? if (!$disable_free_places): ?>
                <div class="prices-box__label">Осталось <? the_field('свободных_мест_оффлайн') ?> мест</div>
            <? endif ?>
            <a href="#order-modal" data-toggle="modal" data-price="<?= $price?:0 ?>" class="btn prices-box__button">Оформить заявку</a>
        </div>
    <? endif ?>
</div>