<?
use Roots\Sage\Assets;

if (is_page_template('templates/landing-1.php'))
    return;
?>
<header class="header">
    <div class="container">
        <div class="header__wrap">
            <a href="/" class="header__logo">
                <img src="<?= Assets\asset_path('images/level-80-logo.svg') ?>" alt="" width="95" height="25" class="header__logo-image" />
            </a>
            <nav class="header__menu">
                <h3 class="sr-only">Главное меню</h3>
                <? wp_nav_menu(['theme_location' => 'primary', 'container' => false]) ?>
            </nav>
            <div class="header__toggle">
                <div class="header__toggle-icon"></div>
            </div>
        </div>
    </div>
</header>