<?

use Roots\Sage\Assets;

?>
<div class="contacts">
	<div class="container">
		<div class="contacts__inner">
			<div class="contacts__title">Еще думаете?</div>
			<div class="contacts__text">Напишите нам и мы развеем все Ваши сомнения :)</div>
			<div class="contacts__image">
				<img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
			</div>
			<div class="contacts__controls">
				<div class="contacts__controls-item"><a href="https://m.me/lvl80" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
				</div>
				<div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
				</div>
			</div>
		</div>
	</div>
</div>