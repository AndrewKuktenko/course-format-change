<div class="nav-list__wrap">
    <? foreach(get_posts(['post_type' => 'kursy', 'posts_per_page' => -1]) as $p): ?>
        <div class="nav-list__item">
            <a href="<?= get_permalink($p) ?>" class="nav-list__link"><?= get_the_title($p) ?></a>
        </div>
    <? endforeach ?>
</div>