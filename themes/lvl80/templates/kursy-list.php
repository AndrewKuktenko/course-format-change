<div <?= is_front_page() ? 'id="section-cources"' : ''?> class="cards-list">
    <div class="container">
        <div class="cards-list__wrap">
            <?
            $args = [
                'post_type' => 'kursy',
                'posts_per_page' => -1,
                'meta_query' => [
                    [
                        'key' => 'отображать_на_сайте',
                        'value' => 1
                    ],
                    'start_date' => [
                        'key' => 'start_date',
                        'type' => 'DATE',
                        'compare' => '>=',
                        'value' => date('Y-m-d')
                    ]
                ],
                'orderby' => 'start_date',
                'order' => 'ASC'
            ];

            if (is_single()) {
                $args['post__not_in'] = [get_the_ID()];
                $args['posts_per_page'] = 2;
            }

            foreach(get_posts($args) as $post) {
	            setup_postdata($post); ?>
	            <div class="cards-list__item">
	                <? get_template_part('templates/kursy', 'item'); ?>
                </div>
            <? }
            wp_reset_postdata();?>
        </div>
    </div>
</div>