<?
/**
 * Template Name: Payment Success
 */
?>
<div style="background-image: url(<?= \Roots\Sage\Assets\asset_path('images/temp/thank-payment-bg.jpg') ?>);" class="page-box">
    <div class="container">
        <h2 class="page-box__title">Спасибо! Ваша оплата принята!</h2>
        <div class="page-box__text">Пока вы ждете начала курса, можете узнать много полезной информации на нашей страничке в Facebook. Обязательно подпишитесь!</div>
        <div class="page-box__buttons"><a href="https://www.facebook.com/lvl80/" class="btn btn-bordered-white page-box__button"><span class="btn-icon"><img src="<?= \Roots\Sage\Assets\asset_path('images/facebook-icon.svg') ?>" alt=""></span>Подписаться</a>
        </div>
    </div>
</div>