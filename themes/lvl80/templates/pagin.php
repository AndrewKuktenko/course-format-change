<? if ($wp_query->max_num_pages <= 1) return; ?>

<div class="pagination">
    <div class="pagination__wrap">
        <? for($i=1;$i <= $wp_query->max_num_pages;$i++): ?>
            <a href="<?= get_pagenum_link( $i ) ?>" class="pagination__item <?= get_query_var('paged') == $i || !get_query_var('paged') && $i == 1  ? 'active' : '' ?>"><?= $i ?></a>
        <? endfor ?>
    </div>
</div>