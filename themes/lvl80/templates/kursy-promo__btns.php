<div class="promo__buttons">
    <div class="promo__buttons-item"><a href="#section-price" class="btn btn-white promo__button scroll-link">Записаться на курс</a></div>
    <div class="promo__buttons-item"><a href="<? the_field('video') ?>" data-fancybox="video" class="btn btn-bordered-white promo__button"><span class="btn-icon-play"></span>Посмотреть лекцию</a></div>
</div>