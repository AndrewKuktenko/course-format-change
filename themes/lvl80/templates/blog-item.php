<div class="posts-box__list-item">
    <div class="post-card">
        <div class="post-card__image">
            <img src="<?= get_wp_user_avatar_src('', 'thumbnail') ?>" alt="" class="post-card__image-i" />
        </div>
        <h3 class="post-card__title">
            <a href="<? the_permalink() ?>" class="post-card__title-link"><? the_title() ?></a>
        </h3>

        <? if(is_front_page()): ?>
            <div class="post-card__text"><? the_excerpt() ?></div>
            <a href="<? the_permalink() ?>" class="post-card__more">подробнее</a>
        <? endif ?>

        <div class="post-card__footer">
            <div class="post-card__author"><? the_author() ?></div>
            <div class="post-card__label"><? the_author_meta('description') ?></div>
        </div>
    </div>
</div>