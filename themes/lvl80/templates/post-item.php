<div data-post-id="<? the_ID() ?>" class="blog-list__item">
	<a href="<? the_permalink() ?>" class="promo-card">
		<span style="background-image: url(<? the_post_thumbnail_url() ?>);" class="promo-card__image"></span>
		<span class="promo-card__content">
            <span class="promo-card__header">
                <span class="promo-card__title"><? the_title() ?></span>
            </span>
            <span class="promo-card__text"><? the_excerpt() ?></span>
            <span class="promo-card__more">Читать</span>
        </span>
	</a>
</div>