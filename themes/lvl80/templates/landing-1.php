<?php
/**
 * Created by PhpStorm.
 * Template Name: Лендинг: Презентации, которые убеждают
 * Date: 015 15.03.18
 * Time: 20:35
 */

use Roots\Sage\Assets;
?>
<div class="intro intro_bg-violet intro_color-mint">
    <div class="container">
        <a href="/" class="intro__logo">
            <img src="<?= Assets\asset_path('images/lvl80-shape-logo.svg') ?>" alt="" class="intro__logo-image" />
        </a>
        <div class="intro__wrap">
            <div class="intro__content">
                <div class="intro__label">Бесплатная онлайн лекция</div>
                <h2 class="intro__title">Презентации, которые убеждают</h2>
                <div class="intro__text">Узнай о том, как подготовить презентацию, которая будет продавать, и выработать уверенность в себе на время выступления</div>
                <div class="intro__thumb">
                    <img src="<?= Assets\asset_path('images/temp/svetlana-lebedeva-promo-thumb.jpg') ?>" alt="">
                </div>
                <div class="intro__info">
                    <div class="intro__info-section">
                        <div class="intro__info-label">Время</div>
                        <div class="intro__info-content">
                            <div class="intro__info-date">
                                <div class="intro__info-date-row">19 марта</div>
                                <div class="intro__info-date-row">19:00 КИЕВ | 20:00 МСК</div>
                            </div>
                        </div>
                    </div>
                    <div class="intro__info-section">
                        <div class="intro__info-label">Лектор</div>
                        <div class="intro__info-content">
                            <div class="intro__info-text">Светлана Лебедева</div>
                            <div class="intro__info-text">маркетинг директор в Shell Retail Ukraine</div>
                        </div>
                    </div>
                </div>
                <div class="intro__footer"><a href="#promo-order-section" class="btn btn-mint intro__button scroll-link">Принять участие</a>
                </div>
            </div>
            <div class="intro__aside">
                <div class="intro__image">
                    <img src="<?= Assets\asset_path('images/temp/svetlana-lebedeva-promo.png') ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="promo-order-section" class="promo-order">
    <div class="container">
        <div class="promo-order__wrap">
            <div class="promo-order__col">
                <div class="video-frame">
                    <video style="width:100%; height: 100%;" preload poster="<?= Assets\asset_path('images/temp/webinar-video-poster.jpg') ?>">
                            <source src="<?= get_template_directory_uri() ?>/static/assets/v/promo.mp4" type="video/mp4">
                        </video>
                    <div class="video-frame__play"></div>
                </div>
                <div class="promo-order__arrow"></div>
            </div>
            <div class="promo-order__col">
                <div class="promo-order__box">
                    <div class="order-box order-box_simple">
                        <div class="validation-wrap">
                            <form novalidate="novalidate" class="order-box__form process_form_1">
                                <input type="hidden" name="action" value="process_form_1">
                                <input type="hidden" name="redirect" value="/webinarcomplete/">

                                <div class="modal-title">Записаться на бесплатную онлайн лекцию</div>
                                <div class="form-group">
                                    <input name="process_form_name" id="field-order-name" type="text" required class="form-control">
                                    <label for="field-order-name" class="control-label">Имя</label>
                                    <div class="error-message">Пожалуйста, введите имя</div>
                                </div>
                                <div class="form-group">
                                    <input name="process_form_phone" id="field-order-phone" type="tel" required class="form-control intl-tel-input">
                                    <div class="error-message">Пожалуйста, введите телефон</div>
                                </div>
                                <div class="form-group">
                                    <input name="process_form_email" id="field-order-email" type="email" required class="form-control">
                                    <label for="field-order-email" class="control-label">Email</label>
                                    <div class="error-message">Пожалуйста, введите действующий e-mail</div>
                                </div>
                                <div class="order-box__form-footer">
                                    <button type="submit" class="btn btn-mint order-box__form-button">Записаться</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="informer informer_bg-white">
    <div class="container">
        <h2 class="informer__title">Чему вы научитесь на лекции</h2>
        <div class="informer__list">
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-1.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Правильно ставить цели презентации</div>
                </div>
            </div>
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-2.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Разговаривать на одном языке со своей аудиторией</div>
                </div>
            </div>
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-3.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Структурировать информацию и создавать storytelling</div>
                </div>
            </div>
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-4.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Визуально удерживать интерес аудитории, создавая крутые слайды</div>
                </div>
            </div>
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-5.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Приводить убедительные доказательства</div>
                </div>
            </div>
            <div class="informer__list-item">
                <div class="informer__list-image">
                    <img src="<?= Assets\asset_path('images/temp/informer-icon-6.svg') ?>" alt="">
                </div>
                <div class="informer__list-content">
                    <div class="informer__list-title">Уверенно выступать и убеждать людей выступая публично</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="teachers-section">
    <div class="container">
        <h2 class="teachers-section__title">Кто читает лекцию</h2>
        <div class="teachers teachers_centered">
            <div class="teachers__item">
                <div class="teachers__item-inner">
                    <div class="teachers__aside">
                        <div class="teachers__image">
                            <img src="<?= Assets\asset_path('images/temp/teachers/svetlana-lebedeva.jpg') ?>" alt="" class="teachers__image-i" />
                        </div>
                        <a href="https://www.facebook.com/sveta.lebedeva.148" target="_blank" class="teachers__social">
                            <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                        </a>
                    </div>
                    <div class="teachers__content">
                        <div class="teachers__title">Лебедева Светлана</div>
                        <div class="teachers__text">14 лет опыта в маркетинге и продажах. Прошла карьерный путь от консультанта по продаже в маленьком рекламном агентстве до маркетинг директора в Shell Retail Ukraine. Верит в то, что маркетинг повсюду, а систематизированный подход к изучению основ
                            продаж и маркетинга, помогает положительно влиять на все сферы жизни человека.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="teachers-section__footer"><a href="#webinar-modal" data-toggle="modal" class="btn btn-mint teachers-section__button">Принять участие</a>
        </div>
    </div>
</div>
<div class="contacts contacts_violet">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите нам и мы развеем все сомнения:)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="https://m.me/lvl80" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
