<div class="projects-list__item">
    <div class="project-card project-card_<?= (is_single() || is_home() ? 'vertical' : 'horizontal') ?>">
        <div style="background-image:url(<? the_post_thumbnail_url('project-thumb') ?>);" class="project-card__image">
            <div class="project-card__image-badge"></div>
        </div>
        <div class="project-card__content">
            <h3 class="project-card__title">
                <a href="<? the_permalink() ?>" class="project-card__title-link"><? the_title() ?></a>
            </h3>

            <? if(is_front_page()): ?>
                <div class="project-card__text"><? the_excerpt() ?></div>
                <a href="<? the_permalink() ?>" class="btn btn-bordered btn-bordered-grey project-card__button">Подробнее</a>
            <? endif ?>
        </div>
    </div>
</div>