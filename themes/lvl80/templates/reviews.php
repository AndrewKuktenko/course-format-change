<?

use Roots\Sage\Assets;

?>
<div class="reviews">
	<div class="container">
		<div class="reviews__inner">
			<h2 class="reviews__title">Отзывы учеников</h2>
			<div class="reviews__text">Мы собрали несколько отзывов наших учеников.<br>Они молодцы!</div>
			<div class="reviews__list">
				<div class="reviews__list-item">
					<a href="https://youtu.be/KrfqscE7zQ4" data-fancybox="video" style="background-image: url('<?= Assets\asset_path('images/temp/reviews-cover-1.jpg?v=2') ?>');" class="reviews__item">
						<span class="reviews__item-icon"></span>
					</a>
					<div class="reviews__list-label"><strong>Ястремский Владимир</strong>, CMO BeerMe</div>
					<div class="reviews__social">
						<a href="https://www.facebook.com/yastremskiyva" target="_blank" class="reviews__social-link">
							<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews__social-image" />
						</a>
					</div>
				</div>
				<div class="reviews__list-item">
					<a href="https://youtu.be/Kczg48NDDw8" data-fancybox="video" style="background-image: url('<?= Assets\asset_path('images/temp/reviews-cover-2.jpg?v=2') ?>');" class="reviews__item">
						<span class="reviews__item-icon"></span>
					</a>
					<div class="reviews__list-label"><strong>Плекан Роман</strong>, CPO eda.ua</div>
					<div class="reviews__social">
						<a href="https://www.facebook.com/romaplek" target="_blank" class="reviews__social-link">
							<img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews__social-image" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>