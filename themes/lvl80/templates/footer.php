<? use Roots\Sage\Assets; ?>

<footer class="footer">
    <div class="container">
        <div class="footer__wrap">
            <div class="footer__socials">
                <a href="https://www.facebook.com/lvl80/" target="_blank" rel="nofollow noopener" class="footer__socials-item">
                    <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" height="19" class="footer__socials-image">
                </a>
                <a href="https://www.instagram.com/lvl80_school/" target="_blank" rel="nofollow noopener" class="footer__socials-item">
                    <img src="<?= Assets\asset_path('images/instagram-icon.svg') ?>" alt="" height="19" class="footer__socials-image">
                </a>
            </div>
            <nav class="footer__menu">
                <h3 class="sr-only">Футер меню</h3>
	            <? wp_nav_menu(['theme_location' => 'footer', 'container' => false]) ?>
            </nav>
        </div>
    </div>
</footer>

<div class="modals">
    <div id="order-modal" tabindex="-1" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button data-dismiss="modal" type="button" class="modal-close icon-close close"></button>
                <div class="order-box">
                    <div class="validation-wrap">
                        <form novalidate="novalidate" class="order-box__form">
                            <input type="hidden" name="action" value="order">
                            <input type="hidden" name="ID" value="<? the_ID() ?>">
                            <input type="hidden" name="field-order-price" id="field-order-price"/>
                            <input type="hidden" name="field-order-type" id="field-order-type"/>
                            <input type="hidden" name="locale" id="locale" value="<?= get_locale() ?>"/>

                            <input type="hidden" id="spam-off" name="spam-off"/>

                            <div class="modal-title">Оформить заявку на обучение</div>
                            <div class="form-group">
                                <input name="field-order-name" id="field-order-name" type="text" required class="form-control">
                                <label for="field-order-name" class="control-label">Имя</label>
                                <div class="error-message">Пожалуйста, введите имя</div>
                            </div>
                            <div class="form-group">
                                <input name="field-order-phone" id="field-order-phone" type="tel" required class="form-control">
                                <label for="field-order-phone" class="control-label">Номер телефона</label>
                                <div class="error-message">Пожалуйста, введите телефон</div>
                            </div>
                            <div class="form-group">
                                <input name="field-order-email" id="field-order-email" type="email" required class="form-control">
                                <label for="field-order-email" class="control-label">Email</label>
                                <div class="error-message">Пожалуйста, введите действующий e-mail</div>
                            </div>
                            <div class="order-box__form-footer">
                                <button type="submit" class="btn order-box__form-button">Зарегистрироваться</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="thanks-callback-modal" tabindex="-1" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <button data-dismiss="modal" type="button" class="modal-close icon-close close"></button>
                <div class="modal-title">Спасибо!</div>
                <div class="modal-text">Мы вам перезвоним :)</div>
            </div>
        </div>
    </div>
    <div id="free-modal" tabindex="-1" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <button data-dismiss="modal" type="button" class="modal-close icon-close close"></button>
                <div class="modal-title">Отлично!</div>
                <div class="modal-text">Ваша заявка принята. Все подробности о курсе уже у вас на почте. Если нет письма, проверьте папку Спам, Промоакции или Реклама :)</div>
            </div>
        </div>
    </div>
    <div id="feedback-modal" tabindex="-1" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button data-dismiss="modal" type="button" class="modal-close icon-close close"></button>
                <div class="feedback-box">
                    <div class="modal-title">У вас есть вопросы?</div>
                    <div class="modal-text">Тогда мы молниеносно на них ответим!</div>
                    <div class="feedback-box__buttons">
                        <div class="feedback-box__buttons-item"><span data-target=".feedback-box__wrap_message" class="btn feedback-box__toggle">Написать нам</span>
                        </div>
                        <div class="feedback-box__buttons-item"><span data-target=".feedback-box__wrap_callback" class="btn btn-bordered-pink feedback-box__toggle">Заказать звонок</span>
                        </div>
                    </div>
                    <div class="feedback-box__wrap feedback-box__wrap_message submit-class">
                        <div class="validation-wrap">
                            <form novalidate="novalidate" class="js-form">
                                <input type="hidden" name="action" value="feedback">
                                <div class="form-group">
                                    <input id="field-feedback-message-name" type="text" required="" name="field-feedback-message-name" class="form-control">
                                    <label for="field-feedback-message-name" class="control-label">Имя</label>
                                    <div class="error-message">Пожалуйста, введите имя</div>
                                </div>
                                <div class="form-group">
                                    <input id="field-feedback-message-email" type="email" required="" name="field-feedback-message-email" class="form-control">
                                    <label for="field-feedback-message-email" class="control-label">Email</label>
                                    <div class="error-message">Пожалуйста, введите действующий e-mail</div>
                                </div>
                                <div class="form-group">
                                    <textarea id="field-feedback-message-text" rows="1" required="" name="field-feedback-message-text" class="form-control" style="overflow: hidden; word-wrap: break-word; height: 28px;"></textarea>
                                    <label for="field-feedback-message-text" class="control-label">Сообщение</label>
                                    <div class="error-message">Пожалуйста, введите сообщение</div>
                                </div>
                                <div class="feedback-box__form-footer">
                                    <div class="feedback-box__form-row">
                                        <button type="submit" class="btn feedback-box__form-button">Отправить</button>
                                    </div>
                                    <div class="feedback-box__form-row"><span data-target=".feedback-box__wrap_callback" class="feedback-box__toggle feedback-box__toggle_link">заказать звонок</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="contacts-box__success">
                            <div class="contacts-box__success-title">Спасибо!</div>
                            <div class="contacts-box__success-text">Ваше сообщение отправлено. Мы его изучим и ответим в течение 24 часов. Даже если это выходной день :)</div>
                        </div>
                    </div>
                    <div class="feedback-box__wrap feedback-box__wrap_callback">
                        <div class="validation-wrap">
                            <form novalidate="novalidate" class="feedback-box__form">
                                <input type="hidden" name="action" value="call_request">
                                <div class="form-group">
                                    <input id="field-feedback-callback-name" type="text" required name="field-feedback-callback-name" class="form-control">
                                    <label for="field-feedback-callback-name" class="control-label">Имя</label>
                                    <div class="error-message">Пожалуйста, введите имя</div>
                                </div>
                                <div class="form-group">
                                    <input id="field-feedback-callback-phone" type="tel" required name="field-feedback-callback-phone" class="form-control intl-tel-input">
                                    <div class="error-message">Пожалуйста, введите телефон</div>
                                </div>
                                <div class="feedback-box__form-footer">
                                    <div class="feedback-box__form-row">
                                        <button type="submit" class="btn feedback-box__form-button">Заказать звонок</button>
                                    </div>
                                    <div class="feedback-box__form-row"><span data-target=".feedback-box__wrap_message" class="feedback-box__toggle feedback-box__toggle_link">написать нам</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="feedback-box__footer">
                        <div class="feedback-box__footer-separator">
                            <div class="feedback-box__footer-label">или</div>
                        </div>
                        <div class="social-networks social-networks_centered">
                            <a href="http://m.me/lvl80" target="_blank" class="social-networks__item">
                                <img src="<?= Assets\asset_path('images/messenger-icon-2.svg') ?>" alt="" width="22" height="22">
                            </a>
                            <a href="http://t.me/v_lokotkov" target="_blank" class="social-networks__item">
                                <img src="<?= Assets\asset_path('images/telegram-icon-2.svg') ?>" alt="" width="22" height="22">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

