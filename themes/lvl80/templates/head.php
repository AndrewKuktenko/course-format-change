<? use Roots\Sage\Assets; ?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#ff0055">
    <link rel="shortcut icon" href="<?= Assets\asset_path('images/favicon.png?v=0') ?>">
    <?php wp_head(); ?>
</head>
