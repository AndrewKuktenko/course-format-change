<?
/**
 * Template Name: Success
 */
?>
<div style="background-image: url('<?= \Roots\Sage\Assets\asset_path('images/temp/thank-order-bg.jpg') ?>');" class="page-box">
    <div class="container">
        <h2 class="page-box__title">Спасибо! Ваша заявка на курс принята.</h2>
        <div class="page-box__text">В течение 24 часов с Вами свяжется наш менеджер.
            <br>А пока вы ждете, посмотрите какие еще курсы у нас есть или подпишитесь на нашу страничку в Facebook. Мы публикуем много полезностей для маркетологов :)</div>
        <div class="page-box__buttons">
            <a href="/#section-cources" class="btn btn-white page-box__button">Посмотреть курсы</a>
            <a href="https://www.facebook.com/lvl80/" class="btn btn-bordered-white page-box__button"><span class="btn-icon"><img src="<?= \Roots\Sage\Assets\asset_path('images/facebook-icon.svg') ?>" alt=""></span>Подписаться</a>
        </div>
    </div>
</div>