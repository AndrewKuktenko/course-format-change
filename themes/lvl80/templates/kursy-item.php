
<a href="<? the_permalink() ?>" class="promo-card">
	<span style="background-image: url(<? the_post_thumbnail_url('kursy-thumb') ?>);" class="promo-card__image"></span>
	<span class="promo-card__content">
        <span class="promo-card__header">
            <span class="promo-card__title"><? the_title() ?></span>
            <span class="promo-card__label">Старт: <? the_field('start_date') ?></span>
        </span>
        <span class="promo-card__text"><?= $post->post_excerpt ?></span>
        <span class="promo-card__more">Подробнее</span>
    </span>
</a>
