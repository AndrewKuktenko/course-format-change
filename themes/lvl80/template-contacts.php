<?php
/**
 *Template Name: Контакты
 */

use Roots\Sage\Assets;
?>

<div class="contacts-box">
    <div class="container">
        <div class="contacts-box__wrap">
            <div class="contacts-box__col submit-class">
                <div class="contacts-box__form">
                    <?= do_shortcode('[contact-form-7 id="26"]') ?>
                </div>
                <div class="contacts-box__success">
                    <div class="contacts-box__success-title">Спасибо!</div>
                    <div class="contacts-box__success-text">Ваше сообщение отправлено. Мы его изучим и ответим в течение 24 часов. Даже если это выходной день :)</div>
                    <div class="btn contacts-box__success-button validation-wrap-reset">Отправить новое сообщение</div>
                </div>
            </div>
            <div class="contacts-box__col">
                <div class="contacts-box__info">
                    <div class="contacts-box__info-section">
                        <div class="contacts-box__info-title">Наши контакты</div>
                        <div class="contacts-box__info-text"><a href="mailto:info@lvl80.pro">info@lvl80.pro</a>
                            <br><a href="callto:+380973745080 ">067 52 16 190</a>
                        </div>
                        <div class="social-networks">
                            <a href="https://www.facebook.com/lvl80/" target="_blank" class="social-networks__item">
                                <img src="<?= Assets\asset_path('images/fb-icon-2.svg') ?>" alt="" width="22" height="22">
                            </a>
                            <a href="https://m.me/lvl80/" target="_blank" class="social-networks__item">
                                <img src="<?= Assets\asset_path('images/messenger-icon-2.svg') ?>" alt="" width="22" height="22">
                            </a>
                            <a href="http://t.me/v_lokotkov" target="_blank" class="social-networks__item">
                                <img src="<?= Assets\asset_path('images/telegram-icon-2.svg') ?>" alt="" width="22" height="22">
                            </a>
                        </div>
                    </div>
                    <div class="contacts-box__info-section">
                        <div class="contacts-box__info-title">Здесь мы проводим занятия</div>
                        <div class="contacts-box__info-text"><strong>INVERIA</strong>
                            <br>ул. Владимирская, 49А
                            <br>Киев, Украина</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="map-wrap">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.655111039122!2d30.512232636895195!3d50.447524446756994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4ce582c884809%3A0xd21afe664c56b938!2z0LLRg9C7LiDQktC-0LvQvtC00LjQvNC40YDRgdGM0LrQsCwgNDnQkCwg0JrQuNGX0LI!5e0!3m2!1sru!2sua!4v1512255225987"
            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
