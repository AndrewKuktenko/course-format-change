/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================
 * ================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'single_kursy': {
            init: function () {

                $('.feedback-box__form').on('validation-submit', function () {

                    $(this).find('.order-box__form-button').prop('disabled', true);

                    $.post (settings.ajax_url, $(this).serializeArray(), function () {
                        window.location.href = '/success/';
                    });
                });

                $('.prices-box__button').on('click', function () {
                    var price = $(this).data('price'),
                        $form = $('.order-box__form');

                    $form.find('#field-order-price').val(price);
                    $form.find('#field-order-type').val($(this).closest('.prices-box__item').data('type'));
                });

            },
            finalize: function () {

            }
        },
        // All pages
        'common': {
            init: function () {

            },
            finalize: function () {

                $('#order-modal').on('shown.bs.modal', function () {
                    $(this).find('#spam-off').val('on');
                });

                $('.order-box__form').on('validation-submit', function () {

                    var $form = $(this);
                    $form.find('.order-box__form-button').prop('disabled', true);

                    var redirect_url = $form.find('input[name=redirect]').length ? $form.find('input[name=redirect]').val() : '/success/';

                    var price = parseInt($('#field-order-price').val());

                    $.post (settings.ajax_url, $(this).serializeArray(), function (response) {
                        if (price === 0) {
                            $('#free-modal').modal('show');
                        } else if (response.success) {
                            window.location.href = response.data.checkout_url;
                        } else {
                            window.location.href = redirect_url;
                        }
                    }, 'json');
                });

                $('.js-form').on('validation-submit', function () {

                    $(this).find('.btn').prop('disabled', true);
                    $.post (settings.ajax_url, $(this).serializeArray());
                    $('#feedback-modal').modal('hide');
                    $('#thanks-callback-modal').modal('show');
                });

                $('.feedback-box__wrap_callback form').on('validation-submit', function () {

                    $(this).find('.order-box__form-button').prop('disabled', true);
                    $('#feedback-modal').modal('hide');
                    $('#thanks-callback-modal').modal('show');
                    $.post (settings.ajax_url, $(this).serializeArray());
                });

                $('a.blog-list__more').click(function (e) {
                    e.preventDefault();

                    var _ = this;

                    var ids = $('[data-post-id]').map(function () {
                        return $(this).data("post-id");
                    }).toArray();

                    var currentTax = $('.tags-list__item.active').first().data('category');

                    $.post(settings.ajax_url, {action: 'get_posts', ids: ids, cat: currentTax}, function(response){
                        if (response.success) {
                            $('.blog-list__wrap').append(response.data.html);

                            if (!response.data.show_more) {
                                _.remove();
                            }
                        }

                    }, 'json');
                });

                $('a[href="#feedback-modal"]').click(function () {
                    $('#feedback-modal').modal('show');
                });

                $('.wpcf7 .form-group').find('input, textarea').each(function () {
                    $(this).parent().wrapInner('<span class="wpcf7-form-control-wrap '+this.getAttribute('name')+'"/>');
                });

                document.addEventListener( 'wpcf7mailsent', function( event ) {
                    $(event.target).closest('.submit-class').addClass('submitted');
                }, false );
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
