<? use Roots\Sage\Assets;?>

<div style="background-image:url(<?= Assets\asset_path('images/temp/smm-promo-cover.jpg') ?>);" class="promo promo_simple promo_violet-2">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Эффективный SMM и таргетинг</h2>
                <div class="promo__text">Забудьте о пустых фразах: “SMM не продает”, “В SMM нельзя ничего отследить” и “SMM — это легко, просто выкладывай фото в Instagram”! Мы докажем, что SMM — это серьезно, сложно и максимально эффективно. Стоимость рекламы в Facebook растет. Управлять кампаниями нужно, как никогда, грамотно. Мы научим тому как выжимать из Facebook максимум! Курс полностью построен на актуальных теоретических и практических навыках, которые помогут стать компетентным специалистом и обеспечат первыми реальными кейсами.</div>
                <div class="promo__label"><strong>Старт:</strong> <? the_field('start_date') ?></div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Этот курс создан для тех, кто уже понял, что SMM-специалист — это серьезный интернет-маркетолог, который разбирается в актуальных трендах социальных сетей, знает и умеет работать с рекламными кабинетами Facebook и методами продвижения брендов, товаров или услуг, полностью осведомлен в необходимых метриках и с легкостью сможет адаптировать функционал социальных сетей под самый нестандартный запрос.</p>
                    <p>Не важно: начинающий ли вы SMM-специалист, бывалый практик маркетинга в социальных сетях или владелец бизнеса, который не уверен, какой канал продвижения ему выбрать — вы пришли по адресу. Мы расскажем вам все самое важное о практической и результативной части социальных сетей.</p>
                </div><a href="#section-schedule" class="btn btn-violet-2 info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/smm-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-smm" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-11.svg') ?>" alt="" width="100" height="100" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Курс "Эффективный SMM и таргетинг"</span><span class="classes__item-label"><strong>Старт:</strong> <? the_field('start_date') ?></span></span></a>
                <div
                        id="class-smm" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-1.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">SMM специалистам,</div>
                                <div class="info-list__text">
                                    <p>которые только начинают работать с социальными сетями или уже имеют опыт работы в SMM и таргетинге, но хотят расширить арсенал знаний и навыков в отрасли.</p>
                                    <p><strong>После прохождения курса:</strong>
                                        <br>Новая профессия или усовершенствование ваших навыков. SMM, как результативный (а иногда и основной) инструмент для формирования и продвижения бренда.</p>
                                </div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-3.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Маркетологам и PR-специалистам,</div>
                                <div class="info-list__text">
                                    <p>которые хотят научиться работать с эффективным каналом продаж и коммуникации бренда.</p>
                                    <p><strong>После прохождения курса:</strong>
                                        <br>Сильный канал формирования бренда и продаж. Умение построения SMM-стратегий, настройка и управление таргетированой рекламой в Facebook, измерение нужных KPI.</p>
                                </div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-6.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Владельцам бизнеса,</div>
                                <div class="info-list__text">
                                    <p>которые хотят разобраться в терминологии, ставить актуальные цели и задачи перед своими специалистами и точно быть уверенными в том, что бюджет не расходуется впустую.</p>
                                    <p><strong>После прохождения курса:</strong>
                                        <br>Навык правильного контроля и проверки подрядчиков, контроля рекламного бюджета, правильного собеседования маркетологов, исчерпывающие знания сферы SMM.</p>
                                </div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-15.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Экспертам, которые хотят зарабатывать онлайн</div>
                                <div class="info-list__text">
                                    <p>Психологи, коучи, консультанты в любой сфере от финансов и бизнеса до здоровья и отношений – практически любую экспертность можно успешно продавать с помощью SMM.</p>
                                    <p><strong>После прохождения курса:</strong>
                                        <br>Мощный инструмент продвижения собственного бренда, построение результативной SMM-стратегии, настройка и управление рекламой в Facebook, лиды и заявки от нужной целевой аудитории.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap"><div class="classes__content-title">Длительность занятий</div><div class="classes__info"><div class="classes__info-item"><div class="classes__info-title">17 сентября</div><div class="classes__info-text">старт курса</div></div><div class="classes__info-item"><div class="classes__info-title">3 месяца</div><div class="classes__info-text">длительность курса</div></div><div class="classes__info-item"><div class="classes__info-title">19:30 – 21:30</div><div class="classes__info-text">в понедельник и среду</div></div><div class="classes__info-item"><div class="classes__info-title">25 занятий</div><div class="classes__info-text">по 2 – 2,5 часа</div></div></div></div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от других?</div>
                    </div>
                    <div class="info-list info-list_simple">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-16.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Запись всех лекций</div>
                                <div class="info-list__text">Все лекции записываются на видео, и студенты могут пересматривать их даже после окончания курса.</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-17.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Поддержка 24/7</div>
                                <div class="info-list__text">К преподавателям можно обращаться с вопросами в любой время суток, а они, в свою очередь, стараются ответить максимально быстро и детально.</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-18.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">10% скидка</div>
                                <div class="info-list__text">Возможность сэкономить при единоразовой оплате курса.</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet-2 scroll-link">Оформить заявку</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чему научитесь на курсе</div>
                    </div>
                    <div class="num-list num-list_violet">
                        <div class="num-list__item">
                            <div class="num-list__num">1</div>
                            <div class="num-list__title">Стратегия продвижения в соцсетях</div>
                            <div class="num-list__text">
                                <p>Составим SMM-стратегию, контент-план, контент-стратегию и коммерческое предложение для заказчика. Проведем анализ целевой аудитории с помощью Facebook Audience Insights, составим портреты целевой аудитории и научимся определять tone of voice для нее. Научимся создавать и оформлять страницы в соцсетях, определять тип контента, усиливать его действие, составлять и оформлять публикации. </p>
                                <p><strong>Результат:</strong>
                                    <br>у вас будет комплексное понимание и все необходимые инструменты для построения стратегии продвижения в соцсетях.</p>
                            </div>
                        </div>
                        <div class="num-list__item">
                            <div class="num-list__num">2</div>
                            <div class="num-list__title">Сервисы, настройка рекламы, лидеры мнений</div>
                            <div class="num-list__text">
                                <p>Проведем тотальный разбор сервисов, поймем на какие не тратить время, а какие – незаменимы, и как использовать их по максимуму. Вникнем в суть работы основных соцсетей: VK, Facebook, Instagram, Twitter, Одноклассники (не спорьте! Там тоже есть ваши клиенты). Научимся запускать и управлять таргетированной рекламой в социальных сетях. Научимся договариваться с площадками, каналами и лидерами мнений. Самостоятельно создадим графику для социальных сетей.</p>
                                <p><strong>Результат:</strong>
                                    <br>вы освоите весь инструментарий профессионального СММ-специалиста.</p>
                            </div>
                        </div>
                        <div class="num-list__item">
                            <div class="num-list__num">3</div>
                            <div class="num-list__title">Аналитика деятельности в соцсетях</div>
                            <div class="num-list__text">
                                <p>Сформируем правильные отчеты для заказчиков и проведем аналитику вашей деятельности –  рекламных кампаний и органической деятельности в соцсети. Научимся анализировать главные KPI в соцсетях и таргетинге.</p>
                                <p><strong>Результат:</strong>
                                    <br>вы будете точно знать какие KPI анализировать, как оценить эффективность деятельности в соцсетях, как правильно составить отчет заказчику.</p>
                            </div>
                        </div>
                        <div class="num-list__item">
                            <div class="num-list__num">4</div>
                            <div class="num-list__title">Разработаем SMM-стратегию для реального клиента</div>
                            <div class="num-list__text">
                                <p>Полученные на курсе знания и инструменты сразу применим на практике. Вы построите реальную SMM-стратегию для клиента, разработаете стратегию таргетированной рекламы, а также получите опыт презентации стратегии заказчику.</p>
                                <p><strong>Результат:</strong>
                                    <br>первая полноценная SMM-стратегия для реального клиента.</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что вас ждет на курсе:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1 schedule-list_violet-2">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Получим базовые знания, необходимые каждому SMM-специалисту</div>
								<div class="schedule-list__text">Изучим основные понятия и термины, которые используются в SMM; поймем какие задачи цели правильные в SMM и почему конверсия должна быть основной целью; разберемся в трендах и основных направлениях SMM.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Научимся создавать и правильно оформлять страницы в социальных сетях</div>
								<div class="schedule-list__text">Разберемся в том, как оформление страницы влияет на конверсию и вовлечение аудитории, на что клиенты обращают внимание и как сделать так, чтоб вашей страницей хотели поделиться с друзьями.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Узнаем какие типы контента существуют в соцсетях и изучим их особенности</div>
								<div class="schedule-list__text">Научимся правильно определять тип контента, усиливать его действие, составлять и оформлять публикации.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Проведем анализ целевой аудитории</div>
								<div class="schedule-list__text">Мы научим вас пользоваться встроенными инструментами для анализа целевой аудитории (Facebook Audience Insights), составлять портреты и аватары целевой аудитории и определять для нее tone of voice.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Самостоятельно создадим графику для социальных сетей</div>
								<div class="schedule-list__text">Вы узнаете особенности создания графических и видеоматериалов в каждой социальной сети, научитесь самостоятельно создавать крутую графику без помощи дизайнера.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разработаем всю необходимую документацию для SMM-проектов</div>
								<div class="schedule-list__text">Мы будем составлять SMM-стратегию, контент-план, контент-стратегию и даже коммерческое предложение для заказчика.</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберем все необходимые для работы SMM-специалиста сервисы и программы</div>
								<div class="schedule-list__text">Проведем тотальный разбор сервисов, узнаем какие из них не очень хорошие, а какие – незаменимы. Мы расскажем и покажем как использовать их по максимуму.</div>
                            </div>
							 <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберем все популярные в СНГ соцсети "по косточкам"</div>
								<div class="schedule-list__text">Вникнем в суть работы всех популярных соц. сетей: VK, Facebook, Instagram, Twitter и даже Одноклассники (да-да, там тоже могут быть ваши клиенты:) ).</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Научимся разрабатывать стратегию продвижения в Facebook</div>
								<div class="schedule-list__text">Разберемся, какие есть возможности продвижения в Facebook и как их использовать стратегически.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Поработаем с Business Manager</div>
								<div class="schedule-list__text">Научимся грамотно создавать учетные записи в Facebook, подключать коллег и агентства к работе, анализировать их работу.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Сделаем тотальный разбор рекламного аккаунта Facebook</div>
								<div class="schedule-list__text">Разберемся во всем функционале рекламного кабинета. Узнаем для чего нужны аудитории, пиксели, отслеживание конверсий, и как все это использовать.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Научимся запускать рекламу в социальных сетях</div>
								<div class="schedule-list__text">Составим аудитории, настроим кампании, научимся сегментировать аудиторию и запускать ремаркетинг.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Изучим рекламу в Instagram</div>
								<div class="schedule-list__text">Разберемся с особенностями и лучшими стратегиями продвижения в Instagram.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Научимся создавать интернет-магазина в Facebook</div>
								<div class="schedule-list__text">Узнаем, как создать интернет-магазин в Facebook и как продвигать товары с вашего сайта в Facebook.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Поработаем с нативной рекламой и лидерами мнений</div>
								<div class="schedule-list__text">Научимся договариваться с площадками, каналами и лидерами мнений; поймем как отличить хорошие площадки от шарлатанов-накрутчиков.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберем в деталях Facebook Messenger</div>
								<div class="schedule-list__text">Научимся создавать чат-бота, вписывать клиентов в воронку, проводить рассылку в Messenger.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Сформируем первые отчеты и проведем аналитику вашей деятельности</div>
								<div class="schedule-list__text">Поймем и даже научимся формировать правильные отчеты для заказчиков, анализировать как рекламные кампании, так и органическую деятельность в социальной сети.</div>
                            </div>
							 <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Изучим основы копирайтинга</div>
								<div class="schedule-list__text">Научимся писать такие посты в SMM, чтоб люди хотели их лайкать, шейрить, комментить и покупать.</div>
                            </div>
							 <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разберем основы видеопродакшена</div>
								<div class="schedule-list__text">Узнаем, как правильно снимать ролики, даже при помощи мобильного телефона, и проводить базовый монтаж видео.</div>
                            </div>
							 <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Научимся создавать креативы</div>
								<div class="schedule-list__text">Научимся делать крутые баннера для рекламы и постов в Facebook без помощи дизайнеров.</div>
                            </div>
							<div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Разработаем SMM-стратегию для реального клиента</div>
								<div class="schedule-list__text">Вы не просто научитесь формировать SMM-стратегию для клиента, но и получите опыт презентации работы перед заказчиком.</div>
                        </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet-2 scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, приглашая только успешных практиков.</div>
                    </div>
                   <div class="teachers">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/viktor-lokotkov.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/olwlo" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Виктор Локотков</div>
                                    <div class="teachers__text">Со-основатель школы LVL80. Более 10 лет опыта в интернет маркетинге. Работал руководителем отдела маркетинга в Genius Marketing, где Facebook является основным источником привлечения клиентов.</div>
                                </div>
                            </div>
                        </div>

                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-davidenko.png') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/j.davydenko" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Давиденко</div>
                                    <div class="teachers__text">Специалист по таргетированной рекламе в LVL80. FB-маркетолог. 4 года практики в интернет-маркетинге. Запуск рекламных кампаний на Украину, Россию, США, Европу, пространство СНГ. То что вы оказались на этом сайте - это тоже ее работа :)</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-kolesnyk.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/julia.noname" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Колесник</div>
                                    <div class="teachers__text">Более 12 лет опыта в сетевых и локальных рекламных агентствах. Copywriter в креативной компании Fedoriv, ранее Senior copywriter в BBDO Ukraine. Клиенты: Toyota, Lexus, MARS, Visa, PEPSI, Nemiroff, Libresse, Samsung.</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/nikita-yabloko.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/appleseater" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Никита Яблоко</div>
                                    <div class="teachers__text">Social Media Manager в YODEZEEN. Опыт работы: 3 года в SMM. Преподавательский стаж - 1 год. Клиенты: Freshline, Nebbia, YODEZEEN, Creamoire, Merida, Neonail, Хуторок</div>
                                </div>
                            </div>
                        </div>
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/protsenko-alexander.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/protsenko.graphics" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Александр Проценко</div>
                                    <div class="teachers__text">Основатель студии веб-дизайна Dvoika, куратор курсов веб-дизайн и граф-дизайн в prjctr.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-violet-2 scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Отзывы студентов</div>
                        <div class="classes__content-text">Ученики, которые уже прошли этот курс, оставили нам отзывы. Смотрите!
</div>
                    </div>
                    <div class="reviews-slider">
                        <div class="reviews-slider__box">
                            <div class="reviews-slider__box-inner"><a href="https://www.youtube.com/embed/QF_9v1XG1as" data-fancybox="video" style="background-image: url('<?= Assets\asset_path('images/temp/reviews-cover-1.jpg') ?>');" class="reviews-slider__item"><span class="reviews-slider__item-icon"></span></a>
                                <div class="reviews-slider__label"><strong>Ястремский Владимир</strong>, CMO BeerMe</div>
                                <div class="reviews-slider__social">
                                    <a href="https://www.facebook.com/ksenya.baranova.3" target="_blank" class="reviews-slider__social-link">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews-slider__social-image" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="reviews-slider__box">
                            <div class="reviews-slider__box-inner">
                                <div class="reviews-slider__photo">
                                    <img src="<?= Assets\asset_path('images/temp/reviews-photo-1.jpg') ?>" alt="" class="reviews-slider__photo-image" />
                                </div>
                                <div class="reviews-slider__message">Проходила курсы в LVL80. Просто супер! Максимальная практика, теории совсем мало. Материал подан на практическом опыте всех лекторов, на примерах их кейсов. Интересные домашние задания, очень глубокие. Лекторы постоянно проверяли их выполение и давали обратную связь. Также мы разрабатывали стратегию для реального клиента. Это безценный опыт. Вся команда была очень отзывчива. Была постоянная поддержка онлайн. Для меня это было очень важно. Отдельно хочу поблагодарить Виктора Локоткова за такую прокачку, новые знания и практику. Благодаря курсам у меня теперь есть еще больше навыков и новые напрвления для развития. Спасибо огромное LVL80! </div>
                                <div class="reviews-slider__label"><strong>Ксюша Баранова</strong>, Product Manager в Lun.ua</div>
                                <div class="reviews-slider__social">
                                    <a href="https://www.facebook.com/ksenya.baranova.3" target="_blank" class="reviews-slider__social-link">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="reviews-slider__social-image" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 3 месяца. Состоит из 24 лекций. Проходит 2 раза в неделю. Можно выбрать онлайн или офлайн-обучение. Количество мест на обоих потоках ограничено.</div>
                    </div>
	                <? get_template_part('templates/kursy', 'price') ?>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Вопросы и ответы</div>
                    </div>
                    <div class="classes__collapse">
                        <div class="collapse-list">
                            <div class="collapse-list__item">
                                <div data-toggle="collapse" data-target="#faq-smm-1" class="collapse-list__item-title collapsed">Получу ли я доступ к видео с курса, если не буду вовремя сдавать домашку?</div>
                                <div id="faq-smm-1" class="collapse">
                                    <div class="collapse-list__item-text">Конечно! Доступ к видео не зависит от выполненных домашних заданий: вы сможете посмотреть все лекции, входящие в курс, а также пересматривать их уже после окончания курса.</div>
                                </div>
                            </div>
                            <div class="collapse-list__item">
                                <div data-toggle="collapse" data-target="#faq-smm-2" class="collapse-list__item-title collapsed">Полагается ли мне какой-нибудь сертификат после окончания курса?</div>
                                <div id="faq-smm-2" class="collapse">
                                    <div class="collapse-list__item-text">
                                        <p>Обязательно! Те студенты, которые выполнили 80% домашних заданий и сдали все тесты, получают сертификат установленного образца.</p>
                                        <p>
                                            <img src='<?= Assets\asset_path('images/temp/certs-image.jpg') ?>' alt='' />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="collapse-list__item">
                                <div data-toggle="collapse" data-target="#faq-smm-3" class="collapse-list__item-title collapsed">В чем разница между онлайновым и оффлайновым форматами обучения?</div>
                                <div id="faq-smm-3" class="collapse">
                                    <div class="collapse-list__item-text">Вы получаете одинаковый объем знаний и доступ к материалам независимо от формы обучения, но именно в оффлайн-формате вы сможете задавать вопросы вживую и общаться с другими студентами.</div>
                                </div>
                            </div>
                            <div class="collapse-list__item">
                                <div data-toggle="collapse" data-target="#faq-smm-4" class="collapse-list__item-title collapsed">Я не могу оплатить весь курс прямо сейчас. Могу ли я платить в рассрочку, за каждый месяц отдельно?</div>
                                <div id="faq-smm-4" class="collapse">
                                    <div class="collapse-list__item-text">Можно платить за каждый месяц обучения по выставленным школой счетам. Но помните, что при разовой оплате курса вы получите скидку в 10%!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? get_template_part('templates/kursy', 'list') ?>

<div class="contacts contacts_violet-2">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите нам и мы развеем все Ваши сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/lvl80pro_bot" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

