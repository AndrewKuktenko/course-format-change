<? global $originalCourse, $price, $url; ?>
<div style="background-image: url(<? the_post_thumbnail_url('full') ?>);" class="request-order">
	<div class="container">
		<div class="request-order__box">
			<h2 class="request-order__title">Спасибо! Ваша заявка на курс принята</h2>
			<div class="request-order__text"><? the_content() ?></div>
			<div class="request-order__image">
				<img src="<?= \Roots\Sage\Assets\asset_path('images/temp/card.svg') ?>" alt="" height="38" class="request-order__image-i">
			</div>
			<?
			$post = $originalCourse;
			?>
			<div class="request-order__payment">
				<div class="request-order__payment-col">
					<div class="request-order__payment-title">Единоразовая оплата</div>
					<div class="request-order__payment-wrap">
						<div class="request-order__payment-price"><?= $price['full'] ?></div>
						<div class="request-order__payment-old"><?= $price['old'] ?></div>
					</div>
					<a href="<?= $url['full'] ?>" class="btn request-order__payment-button">Оплатить весь курс</a>
				</div>
				<div class="request-order__payment-col">
					<div class="request-order__payment-title">Оплата в рассрочку</div>
					<div class="request-order__payment-wrap">
						<div class="request-order__payment-price"><?= $price['part'] ?></div>
					</div>
					<a href="<?= $url['part'] ?>" class="btn request-order__payment-button">Оплатить 1 месяц</a>
				</div>
			</div>
		</div>
	</div>
</div>