<div class="container">
    <h1 class="styled-title">Наши проекты</h1>
    <div class="projects-list projects-list_inline">
        <div class="projects-list__wrap">
            <? while(have_posts()): the_post(); ?>
                <? get_template_part('templates/project', 'item') ?>
            <? endwhile ?>
        </div>
    </div>

    <? get_template_part('templates/pagin') ?>

    <section class="posts-box">
        <h2 class="posts-box__title styled-title">Блоги наших специалистов</h2>
        <div class="posts-box__list">

            <? foreach(get_posts(['numberposts' => 3]) as $post): setup_postdata($post) ?>
                <? get_template_part('templates/blog', 'item') ?>
            <? endforeach;wp_reset_postdata(); ?>

        </div>
        <div class="posts-box__footer">
            <a href="<?= get_the_permalink(get_option('page_for_posts')) ?>" class="btn btn-bordered btn-bordered-grey posts-box__button">Все блоги</a>
        </div>
    </section>
</div>