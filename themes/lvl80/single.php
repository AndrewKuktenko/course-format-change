<? the_post() ?>
<div class="container">
    <div class="page-wrap page-wrap_article">
        <? if(function_exists('yoast_breadcrumb')): ?>
        <div class="breadcrumbs">
            <div class="breadcrumbs__label">Вы находитесь здесь:</div>
            <? yoast_breadcrumb('<div class="breadcrumbs__list">', '</div>') ?>
        </div>
        <? endif ?>
        <div class="page-wrap__container">
            <div class="page-wrap__content">
                <div class="page-wrap__content-inner">
                    <div class="page-wrap__cover">
                        <? the_post_thumbnail('full', ['class' => 'page-wrap__cover-image']) ?>
                    </div>
                    <h1 class="page-wrap__title"><? the_title() ?></h1>
                    <div class="rich-text">
                        <? the_content() ?>
                    </div>
                    <div class="page-wrap__author">
                        <? if(function_exists('get_wp_user_avatar_src')): ?>
                            <div class="page-wrap__author-image">
                                <img src="<?= get_wp_user_avatar_src('', 'avatar') ?>" alt="<? the_author() ?>" width="120" height="120">
                            </div>
                            <div class="page-wrap__author-content">
                                <span class="page-wrap__author-name"><? the_author() ?></span>,&#32;<span class="page-wrap__author-label"><?= get_the_author_meta('description') ?></span>
                            </div>
                        <? endif ?>


                    </div>
                </div>
                <div class="page-wrap__comments">
                    <div id="disqus_thread"></div>
                    <script>
                        (function() {
                            var d = document,
                                s = d.createElement('script');
                            s.src = 'https://lvl80.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                </div>
            </div>
            <? get_sidebar() ?>
        </div>
    </div>
</div>
