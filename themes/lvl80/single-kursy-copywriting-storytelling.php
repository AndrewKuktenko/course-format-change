<? use Roots\Sage\Assets; ?>
<div style="background-image:url('<?= Assets\asset_path('images/temp/copywriting-promo-cover.jpg') ?>');" class="promo promo_green-light promo_sm-content">
    <div class="container">
        <div class="promo__inner">
            <div class="promo__content">
                <h2 class="h1 promo__title">Практический курс по копирайтингу и сторителлингу</h2>
                <div class="promo__text">
                    <p>Два насыщенных месяца теории и практики без перерывов на прокрастинацию.</p>
                    <p>Шестнадцать занятий по освоению эффективных инструментов и проверенных техник написания качественных текстов.</p>
                    <p>Ноль минут пустой болтовни и никаких мотивационных спичей.
                        <br>Строго. Жестко. По существу.</p>
                </div>
	            <? get_template_part('templates/kursy', 'promo__btns') ?>
            </div>
        </div>
    </div>
</div>
<div class="info-box">
    <div class="container">
        <div class="info-box__wrap">
            <div class="info-box__content">
                <div class="info-box__text">
                    <p>Этот курс создан для того, чтобы хорошего текста стало больше. “Позволь себе…”, “Дорогие покупатели…”, “Откройте для себя…” — весь этот незатейливый набор штампов давно пора выкинуть на свалку, равно как и навязчивые рекламные тексты, “впаривающие”,
                        вместо того, чтобы вызывать эмоции и продавать.</p>
                    <p>Копирайтер, пиарщик и любой человек, для которого тексты это ежедневная необходимость, получит на курсе систематизированные знания, рабочие техники и инструменты для того, чтобы сделать рекламный текст качественным, интересным читателю и достигающим
                        поставленных целей.</p>
                </div><a href="#section-schedule" class="btn btn-green-light info-box__button scroll-link">Просмотреть программу курса</a>
            </div>
            <div class="info-box__aside">
                <div class="info-box__image">
                    <img src="<?= Assets\asset_path('images/temp/copywriting-image.jpg') ?>" alt="" class="info-box__image-i" />
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-cources" class="classes classes_offset-bottom-simple">
    <div class="container">
        <div class="classes__list">
            <div class="classes__list-item"><a data-toggle="collapse" href="#class-copywriting" class="classes__item collapsed"><span class="classes__item-image"><img src="<?= Assets\asset_path('images/temp/class-icon-10.svg') ?>" alt="" width="84" height="89" class="classes__item-image-i"/></span><span class="classes__item-content"><span class="classes__item-title">Практический курс по копирайтингу и сторителлингу</span><span class="classes__item-label">Старт <? the_field('start_date') ?></span></span></a>
                <div
                    id="class-copywriting" class="classes__content collapse in">
                    <hr class="no-offset-top">
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Данный курс идеально подойдет:</div>
                    </div>
                    <div class="info-list">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-5.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">I-know-I-can</div>
                                <div class="info-list__text">людям, которые только начинают работать с текстом и хотят получить как понимание, куда двигаться дальше, так и понятные инструменты для работы</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-11.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">I-know-I-can-do-better</div>
                                <div class="info-list__text">людям, которые уже работают с текстом и нуждаются в практических знаниях и навыках для профессионального “рывка вперед”</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Длительность занятий</div>
                        <div class="classes__content-text">Курс стартует 22 октября и длится два месяца.
                            <br>Каждую неделю проходит два занятия — <span class="color-brand">в понедельник и четверг, в 19:30.</span>
                            <br>Занятие длится 2-2,5 часа и состоит из короткой теоретической части и практики.</div>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Чем этот курс отличается от всего другого на рынке?</div>
                    </div>
                    <div class="info-list info-list_simple">
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-12.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Нацеленностью</div>
                                <div class="info-list__text">на передачу знаний</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-13.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Постоянной практикой</div>
                                <div class="info-list__text">как во время занятий, так и самостоятельно по предложенным домашним заданиям</div>
                            </div>
                        </div>
                        <div class="info-list__item">
                            <div class="info-list__image">
                                <img src="<?= Assets\asset_path('images/temp/info-icon-14.svg') ?>" alt="" class="info-list__image-i" />
                            </div>
                            <div class="info-list__content">
                                <div class="info-list__title">Обратной связью</div>
                                <div class="info-list__text">на протяжении всего курса</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green-light scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-schedule" class="classes__content-wrap">
                        <div class="classes__content-title">Что научитесь делать:</div>
                    </div>
                    <div class="schedule-list schedule-list_col-1 schedule-list_green-light">
                        <div class="schedule-list__column">
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Думать, подходить к задаче осознано и системно</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Грамотно работать с фактажем</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Писать текст, вызывающий эмоции у читателя</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Писать длинные и короткие тексты</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Видеть структуру текста, замечать его важные и второстепенные части</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Работать с малой формой — слоганами, хедлайнами</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Определять тон голоса бренда, для которого вы пишете, и работать в его рамках</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Писать истории и пользоваться инструментами драматургии в тексте</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">«Переводить» скучный фактаж в интересные истории</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Создавать сценарии для небольших роликов, прероллов, баннеров</div>
                            </div>
                            <div class="schedule-list__item">
                                <div class="schedule-list__item-icon"></div>
                                <div class="schedule-list__title">Правильно находить задачи и цели текста</div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green-light scroll-link">Записаться на курс</a>
                    </div>
                    <hr>
                    <div class="classes__content-wrap">
                        <div class="classes__content-title">Кто ведет курс</div>
                        <div class="classes__content-text">Мы очень трепетно подходим к выбору преподавателей, приглашая только успешных практиков.</div>
                    </div>
                    <div class="teachers teachers_centered">
                        <div class="teachers__item">
                            <div class="teachers__item-inner">
                                <div class="teachers__aside">
                                    <div class="teachers__image">
                                        <img src="<?= Assets\asset_path('images/temp/teachers/julia-kolesnyk.jpg') ?>" alt="" class="teachers__image-i" />
                                    </div>
                                    <a href="https://www.facebook.com/julia.noname" target="_blank" class="teachers__social">
                                        <img src="<?= Assets\asset_path('images/fb-icon.svg') ?>" alt="" width="10" height="18" class="teachers__social-image" />
                                    </a>
                                </div>
                                <div class="teachers__content">
                                    <div class="teachers__title">Юлия Колесник</div>
                                    <div class="teachers__text">Более 12 лет опыта в сетевых и локальных рекламных агентствах. Copywriter в креативной компании Fedoriv, ранее Senior copywriter в BBDO Ukraine. Клиенты: Toyota, Lexus, MARS, Visa, PEPSI, Nemiroff, Libresse, Samsung и т.д.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="classes__content-wrap"><a href="#section-price" class="btn btn-green-light scroll-link">Оформить заявку на курс</a>
                    </div>
                    <hr>
                    <div id="section-price" class="classes__content-wrap">
                        <div class="classes__content-title">Стоимость курса</div>
                        <div class="classes__content-text">Курс длится 2 месяца. Состоит из 16 лекций. Проходит 2 раза в неделю. Можно выбрать онлайн или офлайн обучение. Количество мест на обоих потоках ограничено.</div>
                    </div>
                    <? get_template_part('templates/kursy', 'price') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? get_template_part('templates/kursy', 'list') ?>
<div class="contacts contacts_green-light">
    <div class="container">
        <div class="contacts__inner">
            <div class="contacts__title">Еще думаете?</div>
            <div class="contacts__text">Напишите нам и мы развеем все Ваши сомнения :)</div>
            <div class="contacts__image">
                <img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
            </div>
            <div class="contacts__controls">
                <div class="contacts__controls-item"><a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="assets/i/messenger-icon.svg" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Messenger</span></a>
                </div>
                <div class="contacts__controls-item"><a href="https://t.me/v_lokotkov" class="btn btn-bordered-white contacts__controls-button"><span class="contacts__controls-icon"><img src="assets/i/telegram-icon.svg" alt="" class="contacts__controls-icon-i"></span><span class="contacts__controls-label">Написать в Telegram</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
