<? use Roots\Sage\Assets; ?>
<div class="info-box info-box_bg-grey info-box_no-shadow">
	<div class="container">
		<div class="info-box__wrap">
			<div class="info-box__content">
				<h2 class="info-box__title">Наши курсы</h2>
				<div class="info-box__text">
					<?php the_field('описание_архивной_для_курсов', 'option'); ?>
				</div>
			</div>
			<div class="info-box__aside">
				<div class="info-box__image">
					<img src="<?= Assets\asset_path('images/temp/books-image.png') ?>" alt="" class="info-box__image-i" />
				</div>
			</div>
		</div>
	</div>
</div>

<? get_template_part('templates/kursy', 'list') ?>

<? get_template_part('templates/reviews') ?>

<div class="contacts">
	<div class="container">
		<div class="contacts__inner">
			<div class="contacts__title">Еще думаете, что бы выбрать?</div>
			<div class="contacts__text">Напишите нам и мы развеем все Ваши сомнения :)</div>
			<div class="contacts__image">
				<img src="<?= Assets\asset_path('images/temp/lvl80-avatar.jpg') ?>" alt="">
			</div>
			<div class="contacts__controls">
				<div class="contacts__controls-item">
                    <a href="https://m.me/ever.again" class="btn btn-bordered-white contacts__controls-button">
                        <span class="contacts__controls-icon">
                            <img src="<?= Assets\asset_path('images/messenger-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/>
                        </span>
                        <span class="contacts__controls-label">Написать в Messenger</span>
                    </a>
				</div>
				<div class="contacts__controls-item">
                    <a href="https://t.me/lvl80pro_bot" class="btn btn-bordered-white contacts__controls-button">
                        <span class="contacts__controls-icon">
                            <img src="<?= Assets\asset_path('images/telegram-icon.svg') ?>" alt="" class="contacts__controls-icon-i"/>
                        </span>
                        <span class="contacts__controls-label">Написать в Telegram</span>
                    </a>
				</div>
			</div>
		</div>
	</div>
</div>