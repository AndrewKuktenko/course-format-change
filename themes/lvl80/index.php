<div class="container">
    <div class="page-wrap page-wrap_blog">
        <div class="page-wrap__search">
            <form action="<?= esc_attr(home_url()) ?>" class="search-field">
                <input value="<?= $_GET['s'] ?? '' ?>" name="s" placeholder="Введите слово для поиска" class="form-control search-field__input">
                <button class="search-field__button"></button>
            </form>
        </div>
	    <? if(have_posts()): ?>
        <ul class="tags-list">
            <li class="<?= is_home() || is_search() ? 'active' : '' ?> tags-list__item">
                <a href="<? the_permalink(get_option('page_for_posts')) ?>" class="tags-list__link">Все темы</a>
            </li>
            <? foreach (get_categories() as $cat): ?>
            <li data-category="<?= $cat->term_id ?>" class="tags-list__item <?= is_category() && get_queried_object_id() === $cat->term_id ? 'active' : '' ?>"><a href="<?= get_term_link($cat) ?>" class="tags-list__link"><?= $cat->name ?></a></li>
            <? endforeach ?>
        </ul>
        <? endif ?>
        <div class="page-wrap__container">
            <div class="page-wrap__content">
                <? if(have_posts()): ?>
                    <div class="blog-list">
                        <div class="blog-list__wrap">
			                <? while(have_posts()): the_post() ?>
				                <? get_template_part('templates/post', 'item') ?>
			                <? endwhile ?>
                        </div>
                        <div class="blog-list__footer">
                            <a href="/" class="blog-list__more">
                                <span class="blog-list__more-label">Загрузить больше статей</span>
                            </a>
                        </div>
                    </div>
                <? else: ?>
                    <p>По вашему запросу не удалось ничего найти :(</p>
                <? endif ?>

            </div>
            <? get_sidebar() ?>
        </div>
    </div>
</div>