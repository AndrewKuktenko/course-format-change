<div class="container">
    <div class="container">
        <div class="service-message">
            <h1 class="service-message__title">Ой…</h1>
            <div class="service-message__text">
                <p>К сожалению, такой страницы нет.</p>
                <p>Но все равно у нас есть много всего интересного :)</p>
            </div>
            <div class="service-message__footer">
                <a href="<?= home_url('/#section-cources') ?>" class="btn service-message__button">Посмотреть курсы</a>
            </div>
            <div class="service-message__image">
                <img src="<?= \Roots\Sage\Assets\asset_path('images/temp/404-img.jpg') ?>" alt="">
            </div>
        </div>
    </div>
</div>